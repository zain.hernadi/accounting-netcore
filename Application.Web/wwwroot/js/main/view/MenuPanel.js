﻿Ext.define('main.view.MenuPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.treemenupanel',
    layout: 'accordion',
    width: 290,
    rootVisible: false,
    useArrows: true,
    autoScroll: true,
    animate: true,
    enableDD: false,
    containerScroll: true,
    header: false,
    split: true,
    /*stateId: 'treemenupanel',
    stateful: true,*/
    multi: true,
    region: 'west',
    collapsible: true,
    hideHeaders: true,
    initComponent: function () {
        var a = this;
        a.dashboardPanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'DASHBOARD',
            rootVisible: false,
            id: 'dashboardpanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-abacus',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'DASHBOARD',
                expanded: true
            }
        });

        a.generalLedgerPanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'GENERAL LEDGER',
            rootVisible: false,
            id: 'generalLedgerPanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-general-ledger',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'GENERAL LEDGER',
                expanded: true
            }
        });

        a.marketingPanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'MARKETING',
            rootVisible: false,
            id: 'marketingPanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-marketing',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'MARKETING',
                expanded: true
            }
        });

        a.purchasePanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'PURCHASING',
            rootVisible: false,
            id: 'purchasePanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-purchasing',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'PURCHASING',
                expanded: true
            }
        });

        a.warehousePanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'WAREHOUSE',
            rootVisible: false,
            id: 'warehousePanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-warehouse',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'WAREHOUSE',
                expanded: true
            }
        });

        a.humanResourcesPanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'HUMAN RESOURCES',
            rootVisible: false,
            id: 'humanResourcesPanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-human-resources',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'HUMAN RESOURCES',
                expanded: true
            }
        });

        a.financePanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'FINANCE',
            rootVisible: false,
            id: 'financePanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-coin',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'FINANCE',
                expanded: true
            }
        });

        a.workflowPanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'WORKFLOW DOCUMENT',
            rootVisible: false,
            id: 'workflowPanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-workflow',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'WORKFLOW DOCUMENT',
                expanded: true
            }
        });

        a.administrationPanel = Ext.create('Ext.tree.Panel', {
            bodyPadding: 0,
            title: 'ADMINISTRATION',
            rootVisible: false,
            id: 'administrationpanel',
            useArrows: true,
            autoScroll: true,
            iconCls: 'icon-administration',
            animate: true,
            enableDD: false,
            containerScroll: true,
            root: {
                text: 'ADMINISTRATION',
                expanded: true
            }
        });

        Ext.apply(a, {
            items: [a.dashboardPanel, a.generalLedgerPanel, a.marketingPanel, a.purchasePanel, a.warehousePanel, a.humanResourcesPanel, a.financePanel, a.workflowPanel, a.administrationPanel],
            listeners: {
                'afterrender': a.onafterrender,
                'itemdblclick': a.onitemdblclick
            }
        });
        a.callParent();
        a.dashboardPanel.on('itemdblclick', a.onitemdblclick);
        a.generalLedgerPanel.on('itemdblclick', a.onitemdblclick);
        a.marketingPanel.on('itemdblclick', a.onitemdblclick);
        a.purchasePanel.on('itemdblclick', a.onitemdblclick);
        a.warehousePanel.on('itemdblclick', a.onitemdblclick);
        a.humanResourcesPanel.on('itemdblclick', a.onitemdblclick);
        a.financePanel.on('itemdblclick', a.onitemdblclick);
        a.workflowPanel.on('itemdblclick', a.onitemdblclick);
        a.administrationPanel.on('itemdblclick', a.onitemdblclick);
    },
    onafterrender: function () {
        var a = this;
        a.dashboardPanel.getRootNode().appendChild([{
            "text": "User Guide",
            "actionType": "",
            "leaf": true,
            "keytag": '',
            "controller": "",
            "view": "",
            "children": null,
            "iconCls": null
        }, {
            "text": "Logout",
            "actionType": "logout",
            "leaf": true,
            "keytag": 'logout',
            "controller": "",
            "view": "logout",
            "children": null,
            "iconCls": null
        }]);

        a.generalLedgerPanel.getRootNode().appendChild([{
            "text": "Master",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Tahun Buku",
                "actionType": "",
                "leaf": true,
                "keytag": 'fiscalyear',
                "controller": "",
                "view": "app.Accounting.Master.FiscalYear.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Account Category",
                "actionType": "",
                "leaf": true,
                "keytag": 'accountcategory',
                "controller": "",
                "view": "app.Accounting.Master.CategoryAccount.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Account Type",
                "actionType": "",
                "leaf": true,
                "keytag": 'accounttype',
                "controller": "",
                "view": "app.Accounting.Master.AccountType.list",
                "children": null,
                "iconCls": null
            },
            //{
            //    "text": "Chart Of Account",
            //    "actionType": "",
            //    "leaf": true,
            //    "keytag": 'chartofaccount',
            //    "controller": "",
            //    "view": "app.GeneralLedger.Base.ChartOfAccount.list",
            //    "children": null,
            //    "iconCls": null
            //},
            {
                "text": "Chart Of Account",
                "actionType": "",
                "leaf": true,
                "keytag": 'chartofaccounttree',
                "controller": "",
                "view": "app.Accounting.Master.ChartOfAccount.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Transaksi",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "General Ledger (GL)",
                "actionType": "multigrid",
                "leaf": true,
                "keytag": 'generalledger;generalledgerdetail',
                "controller": "",
                "view": "app.GeneralLedger.Base.GeneralLedger.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "View History",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Subsidiary Accounts Receivable",
                "actionType": "multigrid",
                "leaf": true,
                "keytag": 'subsidiaryaccountsreceivable;subsidiaryaccountsreceivabledetail',
                "controller": "",
                "view": "app.GeneralLedger.Base.SubsidiaryAccountsReceivable.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Subsidiary Accounts Payable",
                "actionType": "multigrid",
                "leaf": true,
                "keytag": 'subsidiaryaccountspayable;subsidiaryaccountspayabledetail',
                "controller": "",
                "view": "app.GeneralLedger.Base.SubsidiaryAccountsPayable.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Report",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Balance Sheet",
                "actionType": "function",
                "leaf": true,
                "keytag": '',
                "controller": "",
                "view": "app.GeneralLedger.Base.Report.editorBalanceSheet",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }]);

        a.marketingPanel.getRootNode().appendChild([{
            "text": "Master",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Category Customer",
                "actionType": "",
                "leaf": true,
                "keytag": 'categorycustomer',
                "controller": "",
                "view": "app.Accounting.Master.CategoryCustomer.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Customer",
                "actionType": "multigrid",
                "leaf": true,
                "keytag": 'customer;customershipment',
                "controller": "",
                "view": "app.Accounting.Master.Customer.listDashboard",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Transaksi",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Sales Order (SO)",
                "actionType": "",
                "leaf": true,
                "keytag": 'salesorder',
                "controller": "",
                "view": "app.Marketing.Base.SalesOrder.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Sales Invoice (SI)",
                "actionType": "",
                "leaf": true,
                "keytag": 'salesinvoice',
                "controller": "",
                "view": "app.Marketing.Base.SalesInvoice.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Sales Return (SR)",
                "actionType": "",
                "leaf": true,
                "keytag": 'salesreturn',
                "controller": "",
                "view": "app.Marketing.Base.SalesReturn.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Perencanaan Penerimaan Barang (Plan GR)",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Perencanaan Penerimaan Barang (Return)",
                "actionType": "multitab",
                "Uri": "showGoodsReceiptDocumentReturn",
                "leaf": true,
                "keytag": 'warehousegoodsreceiptdocumentsalesreturn',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsReceiptDocument.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }]);

        a.purchasePanel.getRootNode().appendChild([{
            "text": "Master",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Unit Of Measurement",
                "actionType": "",
                "leaf": true,
                "keytag": 'unitofmeasurement',
                "controller": "",
                "view": "app.Purchasing.Base.UnitOfMeasurement.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Courier",
                "actionType": "",
                "leaf": true,
                "keytag": 'courier',
                "controller": "",
                "view": "app.Purchasing.Base.Courier.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Kategori Supplier",
                "actionType": "",
                "leaf": true,
                "keytag": 'categorysupplier',
                "controller": "",
                "view": "app.Accounting.Master.CategorySupplier.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Supplier",
                "actionType": "",
                "leaf": true,
                "keytag": 'supplier',
                "controller": "",
                "view": "app.Accounting.Master.Supplier.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Kategori Material",
                "actionType": "",
                "leaf": true,
                "keytag": 'categorymaterial',
                "controller": "",
                "view": "app.Purchasing.Base.CategoryMaterial.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Material",
                "actionType": "multigrid",
                "leaf": true,
                "keytag": 'material;materialsupplier;materialsupplierprice',
                "controller": "",
                "view": "app.Purchasing.Base.Material.listDashboard",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Transaksi",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Surat Pengajuan Barang (SPB)",
                "actionType": "",
                "leaf": true,
                "keytag": 'materialrequestheader',
                "controller": "",
                "view": "app.Purchasing.Base.MaterialRequest.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Purchase Order (PO)",
                "actionType": "",
                "leaf": true,
                "keytag": 'purchaseorder',
                "controller": "",
                "view": "app.Purchasing.Base.PurchaseOrder.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Purchase Return (PR)",
                "actionType": "",
                "leaf": true,
                "keytag": 'purchaseorderreturn',
                "controller": "",
                "view": "app.Purchasing.Base.PurchaseOrderReturn.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Perencanaan Penerimaan Barang (Plan GR)",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Perencanaan Penerimaan Barang (All)",
                "actionType": "multitab",
                "Uri": "showGoodsReceiptDocumentAll",
                "leaf": true,
                "keytag": 'warehousegoodsreceiptdocument',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsReceiptDocument.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Perencanaan Penerimaan Barang (Purchase)",
                "actionType": "multitab",
                "Uri": "showGoodsReceiptDocumentPurchase",
                "leaf": true,
                "keytag": 'warehousegoodsreceiptdocumentpurchase',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsReceiptDocument.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }]);

        a.warehousePanel.getRootNode().appendChild([{
            "text": "Master",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Transporter",
                "actionType": "",
                "leaf": true,
                "keytag": 'transporter',
                "controller": "",
                "view": "app.Warehouse.Base.Transporter.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Warehouse",
                "actionType": "",
                "leaf": true,
                "keytag": 'warehouse',
                "controller": "",
                "view": "app.Warehouse.Base.Warehouse.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Operasional",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "List Inventory",
                "actionType": "",
                "leaf": true,
                "keytag": 'inventorybalance',
                "controller": "",
                "view": "app.Warehouse.Base.InventoryBalance.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Inbound",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Rencana Penerimaan Barang (GR)",
                "actionType": "multigridtab",
                "Uri": "showGoodsReceipt",
                "leaf": true,
                "keytag": 'warehousegoodsreceiptplan;warehousegoodsreceiptdetailplan;warehousegoodsreceiptqualitycontrolplan',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsReceipt.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Penerimaan Barang (Receiving)",
                "actionType": "multigridtab",
                "Uri": "showGoodsReceiptReceiving",
                "leaf": true,
                "keytag": 'warehousegoodsreceiptreceiving;warehousegoodsreceiptdetailreceiving;warehousegoodsreceiptqualitycontrolreceiving',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsReceipt.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Penerimaan Barang (Quality Control)",
                "actionType": "multigridtab",
                "Uri": "showGoodsReceiptQualityControl",
                "leaf": true,
                "keytag": 'warehousegoodsreceiptqualitycontrol;warehousegoodsreceiptdetailqualitycontrol;warehousegoodsreceiptqualitycontrol',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsReceipt.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Penerimaan Barang (Putaway)",
                "actionType": "multigridtab",
                "Uri": "showGoodsReceiptPutaway",
                "leaf": true,
                "keytag": 'warehousegoodsreceiptputaway;warehousegoodsreceiptdetailputaway;warehousegoodsreceiptqualitycontrolputaway',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsReceipt.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Outbound",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Rencana Pengeluaran Barang",
                "actionType": "",
                "leaf": true,
                "keytag": 'warehouseorderdocument',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseOrderDocument.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "List Allocation",
                "actionType": "",
                "leaf": true,
                "keytag": 'inventoryallocate',
                "controller": "",
                "view": "app.Warehouse.Base.InventoryAllocate.list",
                "children": null,
                "iconCls": null
            }, /*{
                "text": "List Allocation",
                "actionType": "multigrid",
                "leaf": true,
                "keytag": 'outboundallocatedlistdashboard;outbounddetailallocatedlistdashboard;inventoryallocatelistdashboard',
                "controller": "",
                "view": "app.Warehouse.Base.InventoryAllocate.listDashboard",
                "children": null,
                "iconCls": null
            }, */{
                "text": "Pengeluaran Barang (Picking)",
                "actionType": "",
                "leaf": true,
                "keytag": 'warehousepicking',
                "controller": "",
                "view": "app.Warehouse.Base.WarehousePicking.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Pengeluaran Barang (Goods Issue)",
                "actionType": "",
                "leaf": true,
                "keytag": 'warehousegoodsissue',
                "controller": "",
                "view": "app.Warehouse.Base.WarehouseGoodsIssue.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Delivery Order (DO) Warehouse",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Delivery Order (DO)",
                "actionType": "",
                "leaf": true,
                "keytag": 'deliveryorder',
                "controller": "",
                "view": "app.Warehouse.Base.DeliveryOrder.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Proof Of Delivery (POD)",
                "actionType": "multigrid",
                "leaf": true,
                "keytag": 'proofofdeliverysummary;proofofdeliverydetail',
                "controller": "",
                "view": "app.Warehouse.Base.ProofOfDelivery.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }]);

        a.humanResourcesPanel.getRootNode().appendChild([{
            "text": "Master",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Division",
                "actionType": "",
                "leaf": true,
                "keytag": 'division',
                "controller": "",
                "view": "app.HumanResources.Base.Division.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Employee",
                "actionType": "",
                "leaf": true,
                "keytag": 'employee',
                "controller": "",
                "view": "app.HumanResources.Employee.list",
                "children": null,
                "iconCls": null
            }, ],
            "iconCls": null
        }, {
            "text": "Shift Planning",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Holiday Config",
                "actionType": "",
                "leaf": true,
                "keytag": 'holidayconfig',
                "controller": "",
                "view": "app.HumanResources.Base.HolidayConfig.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Shift Config",
                "actionType": "",
                "leaf": true,
                "keytag": 'shiftconfig',
                "controller": "",
                "view": "app.HumanResources.Base.ShiftConfig.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Group Shift",
                "actionType": "",
                "leaf": true,
                "keytag": 'groupshift',
                "controller": "",
                "view": "app.HumanResources.Base.GroupShift.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Attendance",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Attendance",
                "actionType": "",
                "leaf": true,
                "keytag": 'attendance',
                "controller": "",
                "view": "app.HumanResources.Base.Attendance.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Attendance Report",
                "actionType": "",
                "leaf": true,
                "keytag": 'attendancereport',
                "controller": "",
                "view": "app.HumanResources.Base.AttendanceReport.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }]);

        a.financePanel.getRootNode().appendChild([{
            "text": "Master",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Currency",
                "actionType": "",
                "leaf": true,
                "keytag": 'currency',
                "controller": "",
                "view": "app.Accounting.Master.Currency.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Bank",
                "actionType": "",
                "leaf": true,
                "keytag": 'bank',
                "controller": "",
                "view": "app.Accounting.Master.Bank.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Bank Account",
                "actionType": "",
                "leaf": true,
                "keytag": 'bankaccount',
                "controller": "",
                "view": "app.Accounting.Master.BankAccount.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "Pricing Config",
                "actionType": "",
                "leaf": true,
                "keytag": 'pricingconfig',
                "controller": "",
                "view": "app.Finance.Base.PricingConfig.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "Transaksi",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Cash Receipt",
                "actionType": "",
                "leaf": false,
                "controller": "",
                "view": "",
                "children": [{
                    "text": "Cash Receipt (ALL)",
                    "actionType": "multitab",
                    "Uri": "showCashReceiptAll",
                    "leaf": true,
                    "keytag": 'cashreceipt',
                    "controller": "",
                    "view": "app.Finance.Base.CashReceipt.list",
                    "children": null,
                    "iconCls": null
                }, {
                    "text": "Cash Receipt (SALES)",
                    "actionType": "multitab",
                    "Uri": "showCashReceiptSales",
                    "leaf": true,
                    "keytag": 'cashreceiptsales',
                    "controller": "",
                    "view": "app.Finance.Base.CashReceipt.list",
                    "children": null,
                    "iconCls": null
                }],
                "iconCls": null
            }, {
                "text": "Cash Payment",
                "actionType": "",
                "leaf": false,
                "controller": "",
                "view": "",
                "children": [{
                    "text": "Cash Payment (ALL)",
                    "actionType": "multitab",
                    "Uri": "showCashPaymentAll",
                    "leaf": true,
                    "keytag": 'cashpayment',
                    "controller": "",
                    "view": "app.Finance.Base.CashPayment.list",
                    "children": null,
                    "iconCls": null
                }, {
                    "text": "Cash Payment (PURCHASE)",
                    "actionType": "multitab",
                    "Uri": "showCashPaymentPurchase",
                    "leaf": true,
                    "keytag": 'cashpaymentpurchase',
                    "controller": "",
                    "view": "app.Finance.Base.CashPayment.list",
                    "children": null,
                    "iconCls": null
                }],
                "iconCls": null
            }],
            "iconCls": null
            }]);

        a.workflowPanel.getRootNode().appendChild([{
            "text": "Process Workflow",
            "actionType": "",
            "leaf": true,
            "keytag": '',
            "controller": "",
            "view": "",
            "children": null,
            "iconCls": null
        }]);

        a.administrationPanel.getRootNode().appendChild([{
            "text": "Config",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "Code List",
                "actionType": "",
                "leaf": true,
                "keytag": 'codelist',
                "controller": "",
                "view": "app.Administration.Base.CodeList.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }, {
            "text": "User",
            "actionType": "",
            "leaf": false,
            "controller": "",
            "view": "",
            "children": [{
                "text": "User Profile",
                "actionType": "",
                "leaf": true,
                "keytag": 'userprofile',
                "controller": "",
                "view": "app.Authorization.UserProfile.list",
                "children": null,
                "iconCls": null
            }, {
                "text": "User Token",
                "actionType": "",
                "leaf": true,
                "keytag": 'usersession',
                "controller": "",
                "view": "app.Administration.Base.UserSession.list",
                "children": null,
                "iconCls": null
            }],
            "iconCls": null
        }]);

        a.dashboardPanel.expandAll();
        a.generalLedgerPanel.expandAll();
        a.marketingPanel.expandAll();
        a.purchasePanel.expandAll();
        a.warehousePanel.expandAll();
        a.humanResourcesPanel.expandAll();
        a.financePanel.expandAll();
        a.workflowPanel.expandAll();
        a.administrationPanel.expandAll();
    },
    onitemdblclick: function (jview, record) {
        var tab = Ext.getCmp('tabpanelviewid');
        var b = true;
        var items = tab.items.items;
        var keytag;
        var keytags = [];
        if (record.data.actionType === 'multigrid' || record.data.actionType === 'multigridtab') {
            keytags = record.data.keytag.split(';');
            keytag = keytags[0];
        } else {
            keytag = record.data.keytag;
        }

        for (var i = 0; i < items.length; i++) {
            if (items[i].keytag === keytag) {
                tab.setActiveTab(i);
                b = false;
            }
        }
        if (b) {
            if (record.data.view === '') return;
            switch (record.data.actionType) {
                case "logout":
                    k.msg.ask('Are your sure ?', function (btn) {
                        if (btn === 'ok') {
                            window.location = 'Expired';
                        }
                    });
                    break;
                case "window":
                    var win = Ext.create(record.data.view);
                    if (record.data.Uri === '') {
                        win.show();
                    } else {
                        eval("win." + record.data.Uri + "();");
                    }
                    break;
                case "function":
                    var jfunction = Ext.create(record.data.view);
                    jfunction.execute();
                    break;
                case "multigrid":
                    //UserObjectLayout.GetStates(record.data.keytag, function (a, response) {
                    //    var svalue = response.result.result.list;
                    //    var state, state2, state3, state4, state5;
                    //    if (svalue.length > 0 && svalue[0] !== null) {
                    //        state = k.state.decodeValue(svalue[0].ObjectValue);
                    //    }
                    //    if (svalue.length > 1 && svalue[1] !== null) {
                    //        state2 = k.state.decodeValue(svalue[1].ObjectValue);
                    //    }
                    //    if (svalue.length > 2 && svalue[2] !== null) {
                    //        state3 = k.state.decodeValue(svalue[2].ObjectValue);
                    //    }
                    //    if (svalue.length > 3 && svalue[3] !== null) {
                    //        state4 = k.state.decodeValue(svalue[3].ObjectValue);
                    //    }
                    //    if (svalue.length > 4 && svalue[4] !== null) {
                    //        state5 = k.state.decodeValue(svalue[4].ObjectValue);
                    //    }
                    //    var list = Ext.create(record.data.view, {
                    //        keytag: (svalue.length > 0 ? keytags[0] : null),
                    //        keytag2: (svalue.length > 1 ? keytags[1] : null),
                    //        keytag3: (svalue.length > 2 ? keytags[2] : null),
                    //        keytag4: (svalue.length > 3 ? keytags[3] : null),
                    //        keytag5: (svalue.length > 4 ? keytags[4] : null),
                    //        state: state, state2: state2, state3: state3, state4: state4, state5: state5
                    //    });
                    //    tab.add(list);
                    //    tab.setActiveTab(list);
                    //    Ext.state.Manager.provider.ntongDisimpenHeula = false;
                    //});
                    var list = Ext.create(record.data.view, {
                        keytag: keytags[0],
                        keytag2: keytags[1],
                        keytag3: keytags[2],
                        keytag4: keytags[3],
                        keytag5: keytags[4]
                    });
                    tab.add(list);
                    tab.setActiveTab(list);
                    break;
                case "multigridtab":
                    UserObjectLayout.GetStates(record.data.keytag, function (a, response) {
                        var svalue = response.result.result.list;
                        var state, state2, state3, state4, state5;
                        if (svalue.length > 0 && svalue[0] !== null) {
                            state = k.state.decodeValue(svalue[0].ObjectValue);
                        }
                        if (svalue.length > 1 && svalue[1] !== null) {
                            state2 = k.state.decodeValue(svalue[1].ObjectValue);
                        }
                        if (svalue.length > 2 && svalue[2] !== null) {
                            state3 = k.state.decodeValue(svalue[2].ObjectValue);
                        }
                        if (svalue.length > 3 && svalue[3] !== null) {
                            state4 = k.state.decodeValue(svalue[3].ObjectValue);
                        }
                        if (svalue.length > 4 && svalue[4] !== null) {
                            state5 = k.state.decodeValue(svalue[4].ObjectValue);
                        }
                        var multigridtab = Ext.create(record.data.view, {
                            keytag: (svalue.length > 0 ? keytags[0] : null),
                            keytag2: (svalue.length > 1 ? keytags[1] : null),
                            keytag3: (svalue.length > 2 ? keytags[2] : null),
                            keytag4: (svalue.length > 3 ? keytags[3] : null),
                            keytag5: (svalue.length > 4 ? keytags[4] : null),
                            state: state, state2: state2, state3: state3, state4: state4, state5: state5
                        });
                        eval("multigridtab." + record.data.Uri + "();");
                        tab.add(multigridtab);
                        tab.setActiveTab(multigridtab);
                        Ext.state.Manager.provider.ntongDisimpenHeula = false;
                    });
                    break;
                case "multitab":
                    //UserObjectLayout.GetState(record.data.keytag, function (a, response) {
                    //    var svalue = response.result.result.list;
                    //    var state;
                    //    if (svalue !== null) {
                    //        state = k.state.decodeValue(svalue.ObjectValue);
                    //    }
                    //    var multitab = Ext.create(record.data.view, { state: state, keytag: record.data.keytag });
                    //    eval("multitab." + record.data.Uri + "();");
                    //    tab.add(multitab);
                    //    tab.setActiveTab(multitab);
                    //    Ext.state.Manager.provider.ntongDisimpenHeula = false;
                    //});
                    var multitab = Ext.create(record.data.view);
                    eval("multitab." + record.data.Uri + "();");
                    tab.add(multitab);
                    tab.setActiveTab(multitab);
                    break;
                default:
                    //UserObjectLayout.GetState(record.data.keytag, function (a, response) {
                    //    var svalue = response.result.result.list;
                    //    var state;
                    //    if (svalue !== null) {
                    //        state = k.state.decodeValue(svalue.ObjectValue);
                    //    }
                    //    var list = Ext.create(record.data.view, { state: state });
                    //    tab.add(list);
                    //    tab.setActiveTab(list);
                    //    Ext.state.Manager.provider.ntongDisimpenHeula = false;
                    //});
                    var list = Ext.create(record.data.view);
                    tab.add(list);
                    tab.setActiveTab(list);
                    break;
            }
        }
    }
});