﻿Ext.define('main.view.MainView', {
    extend: 'Ext.container.Viewport',
    alias: 'mainview',
    layout: {
        type: 'border'
    },
    requires: [
        'main.view.TabPanelView',
        'main.view.MenuPanel'
    ],
    initComponent: function () {
        var me = this;
        me.buttonLogout = Ext.create('Ext.button.Button', { text: '', iconCls: 'icon-logout', scale: 'small' });
        me.buttonLogout.on('click', me.onLogoutClick, me);
        me.topMenu = Ext.create('Ext.toolbar.Toolbar', {
            id: 'topMenu',
            region: 'north',
            items: [{
                xtype: 'tbtext',
                text: 'BACK OFFICE'
            }, {
                xtype: 'component',
                width: '80%',
                autoEl: {
                    tag: 'span',
                    html: '&nbsp'
                }
            }, {
                xtype: 'tbtext',
                text: 'Welcome, ' + Ext.util.Cookies.get('Username')
            }, {
                xtype: 'component',
                autoEl: {
                    tag: 'span',
                    html: '&nbsp'
                }
            }, me.buttonLogout]
        });
        me.treemenupanel = Ext.widget({
            xtype: 'treemenupanel'
        });
        Ext.applyIf(me, {
            items: [
                me.treemenupanel,
                {
                    xtype: 'tabpanelview'
                },
                //me.topMenu
            ]
        });

        me.callParent(arguments);
        //me.on('afterrender', me.onAfterRender, me);
    },
    onAfterRender: function() {
        Ext.Ajax.request({
            method: 'GET',
            timeout: k.sys.timeout,
            url: 'Home/LoginInfo',
            scope: this,
            success: function (result) {
                var jresult = Ext.JSON.decode(result.responseText);
                this.topMenu.items.items[2].setText('Welcome, ' + jresult.user);
            },
            failure: k.sys.ajaxFailure
        });
    },

    onLogoutClick: function () {
        k.msg.ask('Are your sure ?', function (btn) {
            if (btn === 'ok') {
                window.location = 'Expired';
            }
        });
    }

});