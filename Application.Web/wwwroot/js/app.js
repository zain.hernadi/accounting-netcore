﻿document.oncontextmenu = function () { return true; };
Ext.Loader.setConfig({
    enabled: true
});
Ext.Loader.setPath('Ext.ux', 'js/ext-6.2.0/ux');
Ext.Loader.setPath('app', 'js/app');
Ext.Loader.setPath('base', 'js/base');
Ext.Loader.setPath('main', 'js/main');
Ext.require([
    'main.view.MainView',
    'base.view.MaintainToolbar',
    'base.view.EditorForm',
    'base.view.MaintainForm',
    'base.view.FormParameter'
]);
Ext.onReady(function () {
    //Ext.util.Format.thousandSeparator = '.';
    //Ext.util.Format.decimalSeparator = ',';
    //Ext.util.Format.currencyPrecision = '3';
    //Ext.direct.Manager.addProvider(Ext.app.REMOTING_API);
    var main = Ext.create('main.view.MainView');

    var f = new Ext.state.DatabaseProvider();
    Ext.state.Manager.setProvider(f);

});

