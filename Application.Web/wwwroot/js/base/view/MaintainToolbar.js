﻿Ext.define('base.view.MaintainToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.maintaintoolbar',
    region: 'north',
    resizeable: false,
    initComponent: function () {
        var me = this;
        me.buttonRefresh = k.btn.refresh();
        me.buttonOpen = k.btn.open();
        me.buttonPrint = k.btn.print({hidden: true});
        me.buttonCreate = k.btn.add();
        me.buttonEdit = k.btn.edit({ hidden: true});
        me.buttonDelete = k.btn.erase({ hidden: false });
        me.buttonCancel = k.btn.cancel({ hidden: true });
        me.buttonUpload = k.btn.upload({ hidden: true });
        me.buttonProcess1 = k.btn.process({ hidden: true });
        me.buttonProcess2 = k.btn.process({ hidden: true });
        me.buttonProcess3 = k.btn.process({ hidden: true });
        me.buttonFilter = k.btn.parameter();
        me.buttonSearch = k.btn.search({ text: ' ' });
        me.searchBar = Ext.create('Ext.form.field.Text', { emptyText: 'Cari' });

        Ext.applyIf(me, {
            items: [me.buttonCreate, me.buttonOpen, me.buttonPrint, me.buttonEdit, me.buttonRefresh, me.buttonDelete, me.buttonCancel, me.buttonFilter, me.buttonProcess1, me.buttonProcess2, me.buttonProcess3, me.buttonUpload, me.searchBar, me.buttonSearch]
        });
        me.on('afterrender', me.onafterrender, me);
        me.callParent(arguments);
    },
    onafterrender: function () {
        this.getEl().dom.className += " k-maintainbar-custom-color";
    }
});