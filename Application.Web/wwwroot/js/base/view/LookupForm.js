﻿Ext.define('base.view.LookupForm', {
    extend: 'Ext.window.Window',
    modal: true,
    height: 550,
    width: 750,
    layout: 'fit',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonCancel = k.btn.cancel();
        this.addEvents('showLookup');
        Ext.applyIf(me, {
            buttons: [me.buttonOk, me.buttonCancel]
        });
        me.callParent(arguments);
    }
    , lookup: function () {
        this.fireEvent('showLookup', this);
    }
});