﻿Ext.define('base.view.LoadParameterProfile', {
    extend: 'base.view.MaintainForm',
    title: 'Profile Filter Data',
    layout: 'border',
    keytag: 'userdatafilter',
    keytag2: 'userdatafilteranotheruser',
    closable: true,
    initComponent: function () {
        var me = this;
        me.cstore = Ext.create('base.data.DataFilterStore');
        me.cgrid = Ext.create('Ext.grid.Panel', {
            region: 'center',
            stateful: true,
            stateId: this.keytag,
            //title: 'My Profile',
            enableColumnMove: true,
            store: me.cstore,
            columns: [
                
                { text: 'Filter Name', width: 100, dataIndex: 'FilterName' },
				{ text: 'Object Name', width: 100, dataIndex: 'ObjectName' },
                { text: 'Shared', width: 100, dataIndex: 'Shared' },
                { text: 'Default Param', width: 100, dataIndex: 'SetAsDefaultParam' },
                { text: 'Created By', width: 100, sortable: true, dataIndex: 'CreatedBy' },
                { text: 'Tgl. Create', width: 140, renderer: k.format.renderdatetime, sortable: true, dataIndex: 'CreatedDate' },
                { text: 'Modified By', width: 100, sortable: true, dataIndex: 'ModifiedBy' },
                { text: 'Tgl. Modified', width: 140, renderer: k.format.renderdatetime, sortable: true, dataIndex: 'ModifiedDate' }
            ],
            viewConfig: {
                stripeRows: true,
                loadMask: true,
                loadingText: 'Loading....'
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: me.cstore,
                displayInfo: true,
                displayMsg: 'View Filter Data {0} - {1} of {2}',
                emptyMsg: 'Tidak Ada Data Filter Data'
            })
        });
        if (me.state != null) me.cgrid.applyState(me.state);

        me.mstore = Ext.create('base.data.DataFilterStore');
        me.mgrid = Ext.create('Ext.grid.Panel', {
            region: 'center',
            stateful: true,
            stateId: this.keytag2,
            title: 'Other Profile',
            enableColumnMove: true,
            store: me.mstore,
            columns: [
                { text: 'Filter Name', width: 120, dataIndex: 'FilterName' },
				{ text: 'Object Name', width: 140, dataIndex: 'ObjectName' },
                { text: 'Shared', width: 100, dataIndex: 'Shared' },
                { text: 'Default Param', width: 100, dataIndex: 'SetAsDefaultParam' },
                { text: 'Created By', width: 100, sortable: true, dataIndex: 'CreatedBy' },
                { text: 'Tgl. Create', width: 140, renderer: k.format.renderdatetime, sortable: true, dataIndex: 'CreatedDate' },
                { text: 'Modified By', width: 100, sortable: true, dataIndex: 'ModifiedBy' },
                { text: 'Tgl. Modified', width: 140, renderer: k.format.renderdatetime, sortable: true, dataIndex: 'ModifiedDate' }
            ],
            viewConfig: {
                stripeRows: true,
                loadMask: true,
                loadingText: 'Loading....'
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: me.cstore,
                displayInfo: true,
                displayMsg: 'View Filter Data {0} - {1} of {2}',
                emptyMsg: 'Tidak Ada Data Filter Data'
            })
        });
        me.toolbar = Ext.create('base.view.MaintainToolbar');
        if (me.state2 != null) me.mgrid.applyState(me.state2);
        Ext.applyIf(me, {
            items: []
        });
        me.callParent();
        me.on('afterrender', me.onAfterRender, me);
    },
    onAfterRender: function () {
        var me = this;
        me.toolbar.buttonRefresh.on('click', this.onRefreskClicked, this);
        me.toolbar.buttonFilter.on('click', this.onFilterClicked, this);
        me.toolbar.buttonCreate.hide();
        me.toolbar.buttonOpen.hide();
        me.toolbar.buttonDelete.on('click', this.onButtonDeleteClicked, this);
        me.toolbar.buttonDelete.show();
        me.toolbar.buttonEdit.hide();
        me.toolbar.buttonFilter.hide();
        me.toolbar.searchBar.on('specialkey', me.keyPressSearch, me);
        me.toolbar.buttonSearch.on('click', me.searchData, me);
        me.cgrid.on('celldblclick', me.onGridPanelCellDoubleClicked, me);
        me.cgrid.on('selectionchange', me.onitemselectionchange, me);
        me.mgrid.on('selectionchange', me.onitemselectionchangemgrid, me);
        me.mgrid.on('celldblclick', me.onmGridPanelCellDoubleClicked, me);
        me.onRefreskClicked();
    },
    onRefreskClicked: function () {
        var store = this.cgrid.store;
        if (this.jparams == null) {
            this.jparams = new Array();
            this.jparams.push(this.filterKey);
        }
        var p1 = Ext.JSON.encode(this.jparams);
        var p2 = this.jsorts == null ? '' : Ext.JSON.encode(this.jsorts);
        store.getProxy().setExtraParam('params', p1);
        store.getProxy().setExtraParam('sorters', p2);
        store.getProxy().url = 'UserDataFilter/List';
        store.load({
            scope: this,
            callback: function (records, operation, success) {
                if (success === false) {
                    var jresult = Ext.JSON.decode(operation.serverResponse.responseText);
                    k.msg.error(jresult.msg);
                }
            }
        });

        this.cdata = null;
        //this.cgrid.getSelectionModel().deselectAll();
    },
    onButtonDeleteClicked: function () {
        var me = this;
        if (me.cdata == null) return;
        k.msg.ask('Hapus Data ' + me.cdata.data.FilterName + ' ?', function (btn) {
            if (btn === 'ok') {
                k.msg.wait('Silahkan Tunggu, Proses Penghapusan Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'GET',
                        timeout: k.sys.timeout,
                        params: {
                            id: me.cdata.data.Id
                        },
                        url: 'UserDataFilter/Delete',
                        scope: this,
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);

                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                me.onRefreskClicked();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }
            }
        });
    },
    onGridPanelCellDoubleClicked: function (cgrid, td, cellIndex, record) {
        this.cdata = record;
        this.onButtonOkClicked();
    },
    onmGridPanelCellDoubleClicked: function (cgrid, td, cellIndex, record) {
        this.cdata = record;
        this.onButtonOkClicked();
    },
    onFilterClicked: function () {
        var me = this;
        var a = Ext.create('base.view.FormParameter', {
            dataColumn: [
                ['Nama Kategori Supplier', 'CategoryName', 'string'],
                ['Prioritas', 'Priority', 'string']
            ],
            title: 'Smart Filter - Kategori Supplier'
        });
        a.bok.on('click', function () {
            me.jparams = a.getJsonParam();
            me.jsorts = a.getJsonSort();
            me.onRefreskClicked();
        }, a);
        a.show();
    },
    onEditClicked: function () {
        if (this.cdata == null) return;
        var editor = Ext.create('ufi.Master.CategorySupplier.editor');
        editor.on('afterInsertSuccessEvent', this.onRefreskClicked, this);
        editor.open(this.cdata);
    },
    onitemselectionchange: function (a, selected) {
        if (selected.length > 0)
            this.cdata = selected[0];
    },
    onitemselectionchangemgrid: function (a, selected) {
        if (selected.length > 0)
            this.cdata = selected[0];
    },
    keyPressSearch: function (field, e) {
        if (e.getKey() === Ext.EventObject.ENTER) {
            this.searchData();
        }
    },
    openLoad: function(filterkey) {
        var me = this;
        me.islookup = true;
        me.filterKey = {
            Kolom: 'ObjectName',
            DataType: 'string',
            Operator: 0,
            StringKeyword: filterkey,
            LogicalOperator: 0
        };
        me.buttonOk = k.btn.ok();
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonCancel = k.btn.cancel();
        me.lookupForm = Ext.create('Ext.window.Window', {
            width: 620,
            height: 370,
            modal: true,
            animCollapse: false,
            constrainHeader: true,
            title: 'Profile Filter Data',
            layout: 'border',
            items: [{
                xtype: 'tabpanel', region: 'center',
                activeTab: 0,
                items: [
                {
                    layout: 'border',
                    title: 'My Profile',
                    xtype: 'panel',
                    items: [me.toolbar, me.cgrid]
                }, me.mgrid]
            }],
            buttons: [me.buttonOk, me.buttonCancel]
        });
        me.lookupForm.show();
        me.mgrid.on('activate', function() {
            var store = this.mgrid.store;
            if (this.jparams == null) {
                this.jparams = new Array();
                this.jparams.push(this.filterKey);
            }
            var p1 = this.jparams == null ? '' : Ext.JSON.encode(this.jparams);
            var p2 = this.jsorts == null ? '' : Ext.JSON.encode(this.jsorts);
            store.getProxy().setExtraParam('params', p1);
            store.getProxy().setExtraParam('sorters', p2);
            store.getProxy().url = 'UserDataFilter/ListOtherUser';
            store.load({
                scope: this,
                callback: function (records, operation, success) {
                    if (success === false) {
                        var jresult = Ext.JSON.decode(operation.serverResponse.responseText);
                        k.msg.error(jresult.msg);
                    }
                }
            });

            this.cdata = null;
        }, me);
        me.buttonCancel.on('click', function () {
            me.lookupForm.close();
        }, me);
        me.onAfterRender();
    },
    onButtonOkClicked: function () {
        var me = this;
        // ReSharper disable Html.EventNotResolved
        me.fireEvent('onSelectedItemLookup', me.cdata);
        me.lookupForm.close();
    },
    searchData: function () {
        var me = this;
        me.jparams = new Array();
        me.jparams.push(
        {
            Kolom: 'FilterName',
            DataType: 'string',
            Operator: 4,
            StringKeyword: me.toolbar.searchBar.getValue(),
            LogicalOperator: 0
        }, me.filterKey
        );
        me.onRefreskClicked();
    }
});