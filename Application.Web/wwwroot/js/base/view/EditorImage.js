﻿Ext.define('base.view.EditorImage', {
    extend: 'Ext.window.Window',
    title: 'Gambar',
    iconCls: 'icon-add-material',
    width: 800,
    height: 600,
    modal: true,
    animCollapse: true,
    collapsible: false,
    animateTarget: document.id,
    autoScroll: true,
    maximizable: true,
    initComponent: function () {
        var me = this;
        me.btnClose = k.btn.close();
        me.btnClose.on('click', function () {
            me.fromButtonClose = true;
            if (me.cancelmsg == '') {
                me.close();
                return;
            }
            if (me.cpanel != null && me.cpanel.getForm != null && me.cpanel.getForm().isDirty()) {
                k.msg.ask(me.cancelmsg, function (btn) {
                    if (btn == 'ok') {
                        me.close();
                    }
                });
            } else {
                me.close();
            }
        }, me);
        me.photoPanel = Ext.create('Ext.panel.Panel', {
            layout: 'anchor',
            autoScroll: true
        });
        
        Ext.applyIf(me, {
            items: [
                me.photoPanel
            ],
            buttons: [me.btnClose]
        });
        me.callParent(arguments);
    },

    open: function (record) {
        var me = this;
        me.show();
        me.photoPanel.update(record);
    },
});