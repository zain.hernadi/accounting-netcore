﻿Ext.define('base.view.ActionToolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.actionToolbar',
    region: 'north',
    resizeable: false,
    initComponent: function() {
        var me = this;
        me.buttonRefresh = k.btn.refresh();
        me.buttonSave = k.btn.save();
        me.buttonClose = k.btn.close();
        Ext.applyIf(me, {
            items: [me.buttonSave, me.buttonRefresh, me.buttonClose]
        });
        me.on('afterrender', me.onafterrender, me);
        me.callParent(arguments);
    },
    onafterrender: function () {
        this.getEl().dom.className += " k-toolbar-custom-color";
    }
});