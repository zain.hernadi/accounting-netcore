﻿
(function () {
    Ext.ns('base.form.FormatMoney');

    var UtilFormat = base.form.FormatMoney = {},
        stripTagsRE = /<\/?[^>]+>/gi,
        stripScriptsRe = /(?:<script.*?>)((\n|\r|.)*?)(?:<\/script>)/ig,
        nl2brRe = /\r?\n/g,
        allHashes = /^#+$/,


        formatPattern = /[\d,\.#]+/,


        formatCleanRe = /[^\d\.#]/g,



        I18NFormatCleanRe,


        formatFns = {};

    Ext.apply(UtilFormat, {


        thousandSeparator: ',',




        decimalSeparator: '.',




        currencyPrecision: 2,




        currencySign: '',




        currencyAtEnd: false,



        undef: function (value) {
            return value !== undefined ? value : "";
        },


        defaultValue: function (value, defaultValue) {
            return value !== undefined && value !== '' ? value : defaultValue;
        },


        substr: 'ab'.substr(-1) != 'b'
        ? function (value, start, length) {
            var str = String(value);
            return (start < 0)
                ? str.substr(Math.max(str.length + start, 0), length)
                : str.substr(start, length);
        }
        : function (value, start, length) {
            return String(value).substr(start, length);
        },


        lowercase: function (value) {
            return String(value).toLowerCase();
        },


        uppercase: function (value) {
            return String(value).toUpperCase();
        },


        money: function (v) {
            return UtilFormat.currency(v, '', 2);
        },


        currency: function (v, currencySign, decimals, end) {
            var negativeSign = '',
                format = ",0",
                i = 0;
            v = v - 0;
            if (v < 0) {
                v = -v;
                negativeSign = '-';
            }
            decimals = Ext.isDefined(decimals) ? decimals : UtilFormat.currencyPrecision;
            format += (decimals > 0 ? '.' : '');
            for (; i < decimals; i++) {
                format += '0';
            }
            v = UtilFormat.number(v, format);
            if ((end || UtilFormat.currencyAtEnd) === true) {
                return Ext.String.format("{0}{1}{2}", negativeSign, v, currencySign || UtilFormat.currencySign);
            } else {
                return Ext.String.format("{0}{1}{2}", negativeSign, currencySign || UtilFormat.currencySign, v);
            }
        },


        date: function (v, format) {
            if (!v) {
                return "";
            }
            if (!Ext.isDate(v)) {
                v = new Date(Date.parse(v));
            }
            return Ext.Date.dateFormat(v, format || Ext.Date.defaultFormat);
        },


        dateRenderer: function (format) {
            return function (v) {
                return UtilFormat.date(v, format);
            };
        },


        stripTags: function (v) {
            return !v ? v : String(v).replace(stripTagsRE, "");
        },


        stripScripts: function (v) {
            return !v ? v : String(v).replace(stripScriptsRe, "");
        },


        fileSize: (function () {
            var byteLimit = 1024,
                kbLimit = 1048576,
                mbLimit = 1073741824;

            return function (size) {
                var out;
                if (size < byteLimit) {
                    if (size === 1) {
                        out = '1 byte';
                    } else {
                        out = size + ' bytes';
                    }
                } else if (size < kbLimit) {
                    out = (Math.round(((size * 10) / byteLimit)) / 10) + ' KB';
                } else if (size < mbLimit) {
                    out = (Math.round(((size * 10) / kbLimit)) / 10) + ' MB';
                } else {
                    out = (Math.round(((size * 10) / mbLimit)) / 10) + ' GB';
                }
                return out;
            };
        })(),


        math: (function () {
            var fns = {};

            return function (v, a) {
                if (!fns[a]) {
                    fns[a] = Ext.functionFactory('v', 'return v ' + a + ';');
                }
                return fns[a](v);
            };
        }()),


        round: function (value, precision) {
            var result = Number(value);
            if (typeof precision == 'number') {
                precision = Math.pow(10, precision);
                result = Math.round(value * precision) / precision;
            }
            return result;
        },


        number: function (v, formatString) {
            if (!formatString) {
                return v;
            }
            var formatFn = formatFns[formatString];



            if (!formatFn) {

                var originalFormatString = formatString,
                    comma = UtilFormat.thousandSeparator,
                    decimalSeparator = UtilFormat.decimalSeparator,
                    hasComma,
                    splitFormat,
                    extraChars,
                    precision = 0,
                    multiplier,
                    trimTrailingZeroes,
                    code;





                if (formatString.substr(formatString.length - 2) == '/i') {
                    if (!I18NFormatCleanRe) {
                        I18NFormatCleanRe = new RegExp('[^\\d\\' + UtilFormat.decimalSeparator + ']', 'g');
                    }
                    formatString = formatString.substr(0, formatString.length - 2);
                    hasComma = formatString.indexOf(comma) != -1;
                    splitFormat = formatString.replace(I18NFormatCleanRe, '').split(decimalSeparator);
                } else {
                    hasComma = formatString.indexOf(',') != -1;
                    splitFormat = formatString.replace(formatCleanRe, '').split('.');
                }
                extraChars = formatString.replace(formatPattern, '');

                if (splitFormat.length > 2) {
                } else if (splitFormat.length === 2) {
                    precision = splitFormat[1].length;


                    trimTrailingZeroes = allHashes.test(splitFormat[1]);
                }


                code = [
                    'var utilFormat=base.form.FormatMoney,extNumber=Ext.Number,neg,fnum,parts' +
                        (hasComma ? ',thousandSeparator,thousands=[],j,n,i' : '') +
                        (extraChars ? ',formatString="' + formatString + '",formatPattern=/[\\d,\\.#]+/' : '') +
                        (trimTrailingZeroes ? ',trailingZeroes=/\\.?0+$/;' : ';') +
                    'return function(v){' +
                    'if(typeof v!=="number"&&isNaN(v=extNumber.from(v,NaN)))return"";' +
                    'neg=v<0;',
                    'fnum=Ext.Number.toFixed(Math.abs(v), ' + precision + ');'
                ];

                if (hasComma) {



                    if (precision) {
                        code[code.length] = 'parts=fnum.split(".");';
                        code[code.length] = 'fnum=parts[0];';
                    }
                    code[code.length] =
                        'if(v>=1000) {';
                    code[code.length] = 'thousandSeparator=utilFormat.thousandSeparator;' +
                    'thousands.length=0;' +
                    'j=fnum.length;' +
                    'n=fnum.length%3||3;' +
                    'for(i=0;i<j;i+=n){' +
                        'if(i!==0){' +
                            'n=3;' +
                        '}' +
                        'thousands[thousands.length]=fnum.substr(i,n);' +
                    '}' +
                    'fnum=thousands.join(thousandSeparator);' +
                '}';
                    if (precision) {
                        code[code.length] = 'fnum += utilFormat.decimalSeparator+parts[1];';
                    }

                } else if (precision) {

                    code[code.length] = 'if(utilFormat.decimalSeparator!=="."){' +
                        'parts=fnum.split(".");' +
                        'fnum=parts[0]+utilFormat.decimalSeparator+parts[1];' +
                    '}';
                }

                if (trimTrailingZeroes) {
                    code[code.length] = 'fnum=fnum.replace(trailingZeroes,"");';
                }


                code[code.length] = 'if(neg&&fnum!=="' + (precision ? '0.' + Ext.String.repeat('0', precision) : '0') + '")fnum="-"+fnum;';

                code[code.length] = 'return ';


                if (extraChars) {
                    code[code.length] = 'formatString.replace(formatPattern, fnum);';
                } else {
                    code[code.length] = 'fnum;';
                }
                code[code.length] = '};'

                formatFn = formatFns[originalFormatString] = Ext.functionFactory('Ext', code.join(''))(Ext);
            }
            return formatFn(v);
        },


        numberRenderer: function (format) {
            return function (v) {
                return UtilFormat.number(v, format);
            };
        },


        attributes: function (attributes) {
            if (typeof attributes === 'object') {
                var result = [],
                    name;

                for (name in attributes) {
                    result.push(name, '="', name === 'style' ? Ext.DomHelper.generateStyles(attributes[name]) : Ext.htmlEncode(attributes[name]), '"');
                }
                attributes = result.join('');
            }
            return attributes || '';
        },


        plural: function (v, s, p) {
            return v + ' ' + (v == 1 ? s : (p ? p : s + 's'));
        },


        nl2br: function (v) {
            return Ext.isEmpty(v) ? '' : v.replace(nl2brRe, '<br/>');
        },


        capitalize: Ext.String.capitalize,


        ellipsis: Ext.String.ellipsis,


        format: Ext.String.format,


        htmlDecode: Ext.String.htmlDecode,


        htmlEncode: Ext.String.htmlEncode,


        leftPad: Ext.String.leftPad,


        trim: Ext.String.trim,


        parseBox: function (box) {
            box = box || 0;

            if (typeof box === 'number') {
                return {
                    top: box,
                    right: box,
                    bottom: box,
                    left: box
                };
            }

            var parts = box.split(' '),
                ln = parts.length;

            if (ln == 1) {
                parts[1] = parts[2] = parts[3] = parts[0];
            }
            else if (ln == 2) {
                parts[2] = parts[0];
                parts[3] = parts[1];
            }
            else if (ln == 3) {
                parts[3] = parts[1];
            }

            return {
                top: parseInt(parts[0], 10) || 0,
                right: parseInt(parts[1], 10) || 0,
                bottom: parseInt(parts[2], 10) || 0,
                left: parseInt(parts[3], 10) || 0
            };
        },


        escapeRegex: function (s) {
            return s.replace(/([\-.*+?\^${}()|\[\]\/\\])/g, "\\$1");
        }
    });
}());