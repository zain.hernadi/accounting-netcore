Ext.define('app.Authorization.UserProfile.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true,
    fields: [
        'RecordStatus',
        'Id',
        'RowStatus',
        'Username',
        'Fullname',
        'Telephone',
        'Telephone2',
        'Address',
        'Email',
        { name: 'Sex', type: 'int' },
        { name: 'RegisteredDate', type: 'date' },
        'CreatedBy',
        { name: 'CreatedDate', type: 'date' },
        'ModifiedBy',
        { name: 'ModifiedDate', type: 'date' },
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiBoUrl + 'api/authorization/userprofile/list',
        reader: {
            type: 'json',
            rootProperty: 'ListUserProfile',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

