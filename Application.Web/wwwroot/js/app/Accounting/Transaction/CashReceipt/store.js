Ext.define('app.CashReceipt.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'StatusApproval',
		'EmployeeCode',
		'EmployeeName',
		'DivisionName',
		'Position',
		'Location',
		'Email',
		'CcEmail',
		'CashReceiptNo',
		{ name: 'CashReceiptDate', type: 'date' },
		'CashReceiptStatus',
		'ReceiptDocumentNo',
		'ReceiptDocumentType',
		{ name: 'ReceiptDocumentEffectiveDate', type: 'date' },
		{ name: 'ReceiptDocumentExpiredDate', type: 'date' },
		'ReceiptDocumentRemark',
		'PayerType',
		'PayerCode',
		'PayerName',
		'BankCode',
		'BankName',
		'RefDocumentType',
		'RefDocumentNo',
		{ name: 'RefDocumentDate', type: 'date' },
		'Description',
		'CurrencyCode',
		'TotalPayment',
		'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/cashreceipt/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListCashReceipt',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

