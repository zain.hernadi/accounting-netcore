Ext.define('app.CashReceipt.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 600,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'CashReceipt',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        
		me.StatusApproval = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'StatusApproval', allowBlank: true, name: 'StatusApproval', anchor: '96%' });
		me.EmployeeCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'EmployeeCode', allowBlank: false, name: 'EmployeeCode', anchor: '96%' });
		me.EmployeeName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'EmployeeName', allowBlank: false, name: 'EmployeeName', anchor: '96%' });
		me.DivisionName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'DivisionName', allowBlank: false, name: 'DivisionName', anchor: '96%' });
		me.Position = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Position', allowBlank: false, name: 'Position', anchor: '96%' });
		me.Location = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Location', allowBlank: false, name: 'Location', anchor: '96%' });
		me.Email = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Email', allowBlank: false, name: 'Email', anchor: '96%' });
		me.CcEmail = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'CcEmail', allowBlank: false, name: 'CcEmail', anchor: '96%' });
		me.CashReceiptNo = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'CashReceiptNo', allowBlank: false, name: 'CashReceiptNo', anchor: '96%' });
		me.CashReceiptDate = Ext.create('Ext.form.field.Date', { fieldLabel: 'CashReceiptDate', format: k.format.date, allowBlank: false, name: 'CashReceiptDate', anchor: '96%' });
		me.CashReceiptStatus = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'CashReceiptStatus', allowBlank: false, name: 'CashReceiptStatus', anchor: '96%' });
		me.ReceiptDocumentNo = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'ReceiptDocumentNo', allowBlank: true, name: 'ReceiptDocumentNo', anchor: '96%' });
		me.ReceiptDocumentType = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'ReceiptDocumentType', allowBlank: true, name: 'ReceiptDocumentType', anchor: '96%' });
		me.ReceiptDocumentEffectiveDate = Ext.create('Ext.form.field.Date', { fieldLabel: 'ReceiptDocumentEffectiveDate', format: k.format.date, allowBlank: true, name: 'ReceiptDocumentEffectiveDate', anchor: '96%' });
		me.ReceiptDocumentExpiredDate = Ext.create('Ext.form.field.Date', { fieldLabel: 'ReceiptDocumentExpiredDate', format: k.format.date, allowBlank: true, name: 'ReceiptDocumentExpiredDate', anchor: '96%' });
		me.ReceiptDocumentRemark = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'ReceiptDocumentRemark', allowBlank: true, name: 'ReceiptDocumentRemark', anchor: '96%' });
		me.PayerType = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'PayerType', allowBlank: false, name: 'PayerType', anchor: '96%' });
		me.PayerCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'PayerCode', allowBlank: true, name: 'PayerCode', anchor: '96%' });
		me.PayerName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'PayerName', allowBlank: false, name: 'PayerName', anchor: '96%' });
		me.BankCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'BankCode', allowBlank: true, name: 'BankCode', anchor: '96%' });
		me.BankName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'BankName', allowBlank: true, name: 'BankName', anchor: '96%' });
		me.RefDocumentType = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'RefDocumentType', allowBlank: true, name: 'RefDocumentType', anchor: '96%' });
		me.RefDocumentNo = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'RefDocumentNo', allowBlank: true, name: 'RefDocumentNo', anchor: '96%' });
		me.RefDocumentDate = Ext.create('Ext.form.field.Date', { fieldLabel: 'RefDocumentDate', format: k.format.date, allowBlank: true, name: 'RefDocumentDate', anchor: '96%' });
		me.Description = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 512, fieldLabel: 'Description', allowBlank: false, name: 'Description', anchor: '96%' });
		me.CurrencyCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'CurrencyCode', allowBlank: false, name: 'CurrencyCode', anchor: '96%' });
		me.TotalPayment = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 0, fieldLabel: 'TotalPayment', allowBlank: false, name: 'TotalPayment', anchor: '96%' });
		me.Notes = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 512, fieldLabel: 'Notes', allowBlank: true, name: 'Notes', anchor: '96%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form CashReceipt"
            }, {
                xtype: 'fieldset',
                title: 'CashReceipt Information',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.StatusApproval, me.EmployeeCode, me.EmployeeName, me.DivisionName, me.Position, me.Location, me.Email, me.CcEmail, me.CashReceiptNo, me.CashReceiptDate, me.CashReceiptStatus, me.ReceiptDocumentNo, me.ReceiptDocumentType, me.ReceiptDocumentEffectiveDate, me.ReceiptDocumentExpiredDate, me.ReceiptDocumentRemark, me.PayerType, me.PayerCode, me.PayerName, me.BankCode, me.BankName, me.RefDocumentType, me.RefDocumentNo, me.RefDocumentDate, me.Description, me.CurrencyCode, me.TotalPayment, me.Notes, ]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        /*me.toolbar.buttonRefresh.hide();
        me.toolbar.buttonFilter.hide();
        me.toolbar.buttonOpen.on('click', me.onOpenDetail, me);
        me.toolbar.buttonCreate.on('click', me.onAddDetail, me);
        me.toolbar.buttonDelete.on('click', me.onDeleteDetail, me);
        me.toolbar.buttonEdit.hide();
        me.toolbar.buttonFilter.hide();
        me.toolbar.searchBar.hide();
        me.toolbar.buttonSearch.hide();

        me.GridDetail.on('selectionchange', me.onitemselectionchange, me);
        me.GridDetail.on('celldblclick', me.onOpenDetail, me);*/


        me.insertUrl = k.app.apiAccountingUrl + 'api/cashreceipt/submit';
    },
    onitemselectionchange: function (a, selected) {
        if (selected.length > 0)
            this.cdata = selected[0];
    },
    onAddDetail: function () {
        this.editor = Ext.create('app.CashReceipt.editorDetail');
        this.editor.create();
        this.editor.on('afterInsertSuccessEvent', this.afterAddDetail, this);
    },
    afterAddDetail: function (data) {
        var me = this;
        data.Id = k.sys.data.newGuid();
        data.RowStatus = 0;
        data.CreatedBy = 'new';
        data.CreatedDate = new Date();
        data.ModifiedBy = 'new';
        data.ModifiedDate = new Date();
        data.Crud = k.crudEnum.newest;
        me.StoreDetail.add(data);
    },
    onOpenDetail: function () {
        if (this.cdata === null) {
            return;
        }
        this.editor = Ext.create('app.CashReceipt.editorDetail');
        this.editor.open(this.cdata);
        this.editor.on('afterInsertSuccessEvent', this.afterEditDetail, this);
    },
    afterEditDetail: function (data) {
        var me = this;
        me.StoreDetail.each(function (item) {
            if (item.data.Id === data.Id) {
                if (item.bakdata === null) {
                    item.bakdata = item.data;
                }
                item.beginEdit();
                if (item.data.Crud === k.crudEnum.newest) {
                }
                else if (item.data.Crud === k.crudEnum.edited) {
                }
                else if (item.data.Crud === k.crudEnum.deleted) {
                    item.data.Crud = k.crudEnum.edited;
                }
                else if (item.data.Crud === k.crudEnum.selected) {
                    item.data.Crud = k.crudEnum.edited;
                }
                item.endEdit();
                item.commit();
            }
        }, this);
    },
    onDeleteDetail: function () {
        var me = this;
        if (me.cdata === null) return;
        k.msg.ask('Delete data ' + me.cdata.data.Id + ' ?', function (btn) {
            if (btn === 'ok') {
                if (me.cdata.data.Crud === k.crudEnum.newest) {
                    me.StoreDetail.remove(me.cdata);
                } else {
                    me.cdata.beginEdit();
                    me.cdata.data.Crud = k.crudEnum.deleted;
                    me.cdata.endEdit();
                    me.cdata.commit();
                }
            }
        });
    },
    
    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input CashReceipt');
        me.cancelmsg = 'Cancel Input CashReceipt ?';

        me.confirm = 'Save Data CashReceipt ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update CashReceipt ?';
        me.setTitle('Update CashReceipt');

        me.confirm = 'Update Data CashReceipt ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close CashReceipt ?';
        me.setTitle('CashReceipt');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.StatusApproval.setReadOnly(true); 
		me.EmployeeCode.setReadOnly(true); 
		me.EmployeeName.setReadOnly(true); 
		me.DivisionName.setReadOnly(true); 
		me.Position.setReadOnly(true); 
		me.Location.setReadOnly(true); 
		me.Email.setReadOnly(true); 
		me.CcEmail.setReadOnly(true); 
		me.CashReceiptNo.setReadOnly(true); 
		me.CashReceiptDate.setReadOnly(true); 
		me.CashReceiptStatus.setReadOnly(true); 
		me.ReceiptDocumentNo.setReadOnly(true); 
		me.ReceiptDocumentType.setReadOnly(true); 
		me.ReceiptDocumentEffectiveDate.setReadOnly(true); 
		me.ReceiptDocumentExpiredDate.setReadOnly(true); 
		me.ReceiptDocumentRemark.setReadOnly(true); 
		me.PayerType.setReadOnly(true); 
		me.PayerCode.setReadOnly(true); 
		me.PayerName.setReadOnly(true); 
		me.BankCode.setReadOnly(true); 
		me.BankName.setReadOnly(true); 
		me.RefDocumentType.setReadOnly(true); 
		me.RefDocumentNo.setReadOnly(true); 
		me.RefDocumentDate.setReadOnly(true); 
		me.Description.setReadOnly(true); 
		me.CurrencyCode.setReadOnly(true); 
		me.TotalPayment.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/cashreceipt/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.CashReceipt);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


