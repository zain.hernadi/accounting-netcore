Ext.define('app.GeneralLedger.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'StatusApproval',
		'EmployeeCode',
		'EmployeeName',
		'DivisionName',
		'Position',
		'Location',
		'Email',
		'CcEmail',
		'FiscalYearCode',
		'GeneralLedgerNo',
		'GeneralLedgerName',
		{ name: 'GeneralLedgerDate', type: 'date' },
		'GeneralLedgerType',
		'ReffDocumentCode',
		'ReffDocumentName',
		{ name: 'TransactionDate', type: 'date' },
		'TransactionType',
		'CurrencyCode',
		'TotalDebet',
		'TotalCredit',
		{ name: 'PostingDate', type: 'date' },
		'IsLastBalance',
		'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/generalledger/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListGeneralLedger',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

