Ext.define('app.SubsidiaryAccountsPayable.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'SupplierCode',
		'SupplierName',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/subsidiaryaccountspayable/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListSubsidiaryAccountsPayable',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

