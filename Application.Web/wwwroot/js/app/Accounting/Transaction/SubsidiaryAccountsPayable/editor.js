Ext.define('app.SubsidiaryAccountsPayable.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 600,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'SubsidiaryAccountsPayable',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        
		me.SupplierCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'SupplierCode', allowBlank: false, name: 'SupplierCode', anchor: '96%' });
		me.SupplierName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'SupplierName', allowBlank: false, name: 'SupplierName', anchor: '96%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form SubsidiaryAccountsPayable"
            }, {
                xtype: 'fieldset',
                title: 'SubsidiaryAccountsPayable Information',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.SupplierCode, me.SupplierName, ]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        /*me.toolbar.buttonRefresh.hide();
        me.toolbar.buttonFilter.hide();
        me.toolbar.buttonOpen.on('click', me.onOpenDetail, me);
        me.toolbar.buttonCreate.on('click', me.onAddDetail, me);
        me.toolbar.buttonDelete.on('click', me.onDeleteDetail, me);
        me.toolbar.buttonEdit.hide();
        me.toolbar.buttonFilter.hide();
        me.toolbar.searchBar.hide();
        me.toolbar.buttonSearch.hide();

        me.GridDetail.on('selectionchange', me.onitemselectionchange, me);
        me.GridDetail.on('celldblclick', me.onOpenDetail, me);*/


        me.insertUrl = k.app.apiAccountingUrl + 'api/subsidiaryaccountspayable/submit';
    },
    onitemselectionchange: function (a, selected) {
        if (selected.length > 0)
            this.cdata = selected[0];
    },
    onAddDetail: function () {
        this.editor = Ext.create('app.SubsidiaryAccountsPayable.editorDetail');
        this.editor.create();
        this.editor.on('afterInsertSuccessEvent', this.afterAddDetail, this);
    },
    afterAddDetail: function (data) {
        var me = this;
        data.Id = k.sys.data.newGuid();
        data.RowStatus = 0;
        data.CreatedBy = 'new';
        data.CreatedDate = new Date();
        data.ModifiedBy = 'new';
        data.ModifiedDate = new Date();
        data.Crud = k.crudEnum.newest;
        me.StoreDetail.add(data);
    },
    onOpenDetail: function () {
        if (this.cdata === null) {
            return;
        }
        this.editor = Ext.create('app.SubsidiaryAccountsPayable.editorDetail');
        this.editor.open(this.cdata);
        this.editor.on('afterInsertSuccessEvent', this.afterEditDetail, this);
    },
    afterEditDetail: function (data) {
        var me = this;
        me.StoreDetail.each(function (item) {
            if (item.data.Id === data.Id) {
                if (item.bakdata === null) {
                    item.bakdata = item.data;
                }
                item.beginEdit();
                if (item.data.Crud === k.crudEnum.newest) {
                }
                else if (item.data.Crud === k.crudEnum.edited) {
                }
                else if (item.data.Crud === k.crudEnum.deleted) {
                    item.data.Crud = k.crudEnum.edited;
                }
                else if (item.data.Crud === k.crudEnum.selected) {
                    item.data.Crud = k.crudEnum.edited;
                }
                item.endEdit();
                item.commit();
            }
        }, this);
    },
    onDeleteDetail: function () {
        var me = this;
        if (me.cdata === null) return;
        k.msg.ask('Delete data ' + me.cdata.data.Id + ' ?', function (btn) {
            if (btn === 'ok') {
                if (me.cdata.data.Crud === k.crudEnum.newest) {
                    me.StoreDetail.remove(me.cdata);
                } else {
                    me.cdata.beginEdit();
                    me.cdata.data.Crud = k.crudEnum.deleted;
                    me.cdata.endEdit();
                    me.cdata.commit();
                }
            }
        });
    },
    
    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input SubsidiaryAccountsPayable');
        me.cancelmsg = 'Cancel Input SubsidiaryAccountsPayable ?';

        me.confirm = 'Save Data SubsidiaryAccountsPayable ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update SubsidiaryAccountsPayable ?';
        me.setTitle('Update SubsidiaryAccountsPayable');

        me.confirm = 'Update Data SubsidiaryAccountsPayable ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close SubsidiaryAccountsPayable ?';
        me.setTitle('SubsidiaryAccountsPayable');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.SupplierCode.setReadOnly(true); 
		me.SupplierName.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/subsidiaryaccountspayable/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.SubsidiaryAccountsPayable);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


