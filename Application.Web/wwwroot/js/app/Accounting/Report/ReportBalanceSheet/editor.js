Ext.define('app.ReportBalanceSheet.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 600,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'ReportBalanceSheet',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        
		me.SessionId = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 0, fieldLabel: 'SessionId', allowBlank: false, name: 'SessionId', anchor: '96%' });
		me.LineNumber = Ext.create('Ext.form.field.Number', { fieldLabel: 'LineNumber', allowBlank: false, name: 'LineNumber',hideTrigger: true, anchor: '96%' });
		me.AccountCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'AccountCode', allowBlank: false, name: 'AccountCode', anchor: '96%' });
		me.AccountName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'AccountName', allowBlank: false, name: 'AccountName', anchor: '96%' });
		me.AccountLevel = Ext.create('Ext.form.field.Number', { fieldLabel: 'AccountLevel', allowBlank: false, name: 'AccountLevel',hideTrigger: true, anchor: '96%' });
		me.IsAccountDetail = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 0, fieldLabel: 'IsAccountDetail', allowBlank: false, name: 'IsAccountDetail', anchor: '96%' });
		me.Debet = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 0, fieldLabel: 'Debet', allowBlank: false, name: 'Debet', anchor: '96%' });
		me.Credit = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 0, fieldLabel: 'Credit', allowBlank: false, name: 'Credit', anchor: '96%' });
		me.Saldo = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 0, fieldLabel: 'Saldo', allowBlank: false, name: 'Saldo', anchor: '96%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form ReportBalanceSheet"
            }, {
                xtype: 'fieldset',
                title: 'ReportBalanceSheet Information',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.SessionId, me.LineNumber, me.AccountCode, me.AccountName, me.AccountLevel, me.IsAccountDetail, me.Debet, me.Credit, me.Saldo, ]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        /*me.toolbar.buttonRefresh.hide();
        me.toolbar.buttonFilter.hide();
        me.toolbar.buttonOpen.on('click', me.onOpenDetail, me);
        me.toolbar.buttonCreate.on('click', me.onAddDetail, me);
        me.toolbar.buttonDelete.on('click', me.onDeleteDetail, me);
        me.toolbar.buttonEdit.hide();
        me.toolbar.buttonFilter.hide();
        me.toolbar.searchBar.hide();
        me.toolbar.buttonSearch.hide();

        me.GridDetail.on('selectionchange', me.onitemselectionchange, me);
        me.GridDetail.on('celldblclick', me.onOpenDetail, me);*/


        me.insertUrl = k.app.apiAccountingUrl + 'api/reportbalancesheet/submit';
    },
    onitemselectionchange: function (a, selected) {
        if (selected.length > 0)
            this.cdata = selected[0];
    },
    onAddDetail: function () {
        this.editor = Ext.create('app.ReportBalanceSheet.editorDetail');
        this.editor.create();
        this.editor.on('afterInsertSuccessEvent', this.afterAddDetail, this);
    },
    afterAddDetail: function (data) {
        var me = this;
        data.Id = k.sys.data.newGuid();
        data.RowStatus = 0;
        data.CreatedBy = 'new';
        data.CreatedDate = new Date();
        data.ModifiedBy = 'new';
        data.ModifiedDate = new Date();
        data.Crud = k.crudEnum.newest;
        me.StoreDetail.add(data);
    },
    onOpenDetail: function () {
        if (this.cdata === null) {
            return;
        }
        this.editor = Ext.create('app.ReportBalanceSheet.editorDetail');
        this.editor.open(this.cdata);
        this.editor.on('afterInsertSuccessEvent', this.afterEditDetail, this);
    },
    afterEditDetail: function (data) {
        var me = this;
        me.StoreDetail.each(function (item) {
            if (item.data.Id === data.Id) {
                if (item.bakdata === null) {
                    item.bakdata = item.data;
                }
                item.beginEdit();
                if (item.data.Crud === k.crudEnum.newest) {
                }
                else if (item.data.Crud === k.crudEnum.edited) {
                }
                else if (item.data.Crud === k.crudEnum.deleted) {
                    item.data.Crud = k.crudEnum.edited;
                }
                else if (item.data.Crud === k.crudEnum.selected) {
                    item.data.Crud = k.crudEnum.edited;
                }
                item.endEdit();
                item.commit();
            }
        }, this);
    },
    onDeleteDetail: function () {
        var me = this;
        if (me.cdata === null) return;
        k.msg.ask('Delete data ' + me.cdata.data.Id + ' ?', function (btn) {
            if (btn === 'ok') {
                if (me.cdata.data.Crud === k.crudEnum.newest) {
                    me.StoreDetail.remove(me.cdata);
                } else {
                    me.cdata.beginEdit();
                    me.cdata.data.Crud = k.crudEnum.deleted;
                    me.cdata.endEdit();
                    me.cdata.commit();
                }
            }
        });
    },
    
    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input ReportBalanceSheet');
        me.cancelmsg = 'Cancel Input ReportBalanceSheet ?';

        me.confirm = 'Save Data ReportBalanceSheet ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update ReportBalanceSheet ?';
        me.setTitle('Update ReportBalanceSheet');

        me.confirm = 'Update Data ReportBalanceSheet ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close ReportBalanceSheet ?';
        me.setTitle('ReportBalanceSheet');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.SessionId.setReadOnly(true); 
		me.LineNumber.setReadOnly(true); 
		me.AccountCode.setReadOnly(true); 
		me.AccountName.setReadOnly(true); 
		me.AccountLevel.setReadOnly(true); 
		me.IsAccountDetail.setReadOnly(true); 
		me.Debet.setReadOnly(true); 
		me.Credit.setReadOnly(true); 
		me.Saldo.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/reportbalancesheet/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.ReportBalanceSheet);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


