Ext.define('app.ReportBalanceSheet.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'SessionId',
		{ name: 'LineNumber', type: 'int' },
		'AccountCode',
		'AccountName',
		{ name: 'AccountLevel', type: 'int' },
		'IsAccountDetail',
		'Debet',
		'Credit',
		'Saldo',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/reportbalancesheet/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListReportBalanceSheet',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

