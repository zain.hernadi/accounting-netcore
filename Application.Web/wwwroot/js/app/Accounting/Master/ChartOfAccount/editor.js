Ext.define('app.Accounting.Master.ChartOfAccount.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 650,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Chart Of Account',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        me.ParentId = Ext.create('Ext.form.field.Hidden', { name: 'ParentId' });
        me.SchemaCode = Ext.create('Ext.form.field.Hidden', { name: 'SchemaCode', value: "0000" });

        me.CategoryStore = Ext.create('app.Accounting.Master.CategoryAccount.store');
        me.CategoryStore.on('beforeload', function (store) {
            store.getProxy().url = k.app.apiAccountingUrl + 'api/accounting/categoryaccount/list';
            store.getProxy().setExtraParam('params', 'null');
        }, me);
        me.AccountCategory = Ext.create('Ext.form.field.ComboBox', {
            enforceMaxLength: true, fieldLabel: 'Account Category', allowBlank: false, name: 'AccountCategory', anchor: '96%', editable: false,
            queryMode: 'remote',
            typeAhead: false,
            hideTrigger: false,
            forceSelection: false,
            enableKeyEvents: true,
            valueField: 'AccountCategoryName',
            displayField: 'AccountCategoryName',
            store: me.CategoryStore,
            listeners: {
                change: function (ele, newValue, oldValue) {
                    if (newValue !== oldValue && newValue !== ele.lastSelectEvent) {
                        this.store.data.items.forEach(function (e) {
                            if (e.data.AccountCategoryName === newValue) {
                                dum = e.data.Id;
                                return;
                            }
                        });
                        me.CategoryAccountId.setValue(dum);
                    }
                }
            }
        });
        me.CategoryAccountId = Ext.create('Ext.form.field.Hidden', { name: 'CategoryAccountId' });


        me.TypeStore = Ext.create('app.Accounting.Master.AccountType.store');
        me.TypeStore.on('beforeload', function (store) {
            store.getProxy().url = k.app.apiAccountingUrl + 'api/accounting/accounttype/list';
            store.getProxy().setExtraParam('params', 'null');
        }, me);
        me.AccountType = Ext.create('Ext.form.field.ComboBox', {
            enforceMaxLength: true, fieldLabel: 'Account Type', allowBlank: false, name: 'AccountType', anchor: '96%', editable: false,
            queryMode: 'remote',
            typeAhead: false,
            hideTrigger: false,
            forceSelection: false,
            enableKeyEvents: true,
            valueField: 'AccountTypeName',
            displayField: 'AccountTypeName',
            store: me.TypeStore,
            listeners: {
                change: function (ele, newValue, oldValue) {
                    if (newValue !== oldValue && newValue !== ele.lastSelectEvent) {
                        this.store.data.items.forEach(function (e) {
                            if (e.data.AccountTypeName === newValue) {
                                dum = e.data.Id;
                                return;
                            }
                        });
                        me.AccountTypeId.setValue(dum);
                    }
                }
            }
        });
        me.AccountTypeId = Ext.create('Ext.form.field.Hidden', { name: 'AccountTypeId' });

        me.TreeAccount = Ext.create('Ext.tree.Panel', {
            title: 'Account Parent',
            anchor: '96%',
            height: 470,
            collapsible: false,
            useArrows: true,
            rootVisible: false
        });

        me.IsAccountDetail = Ext.create('Ext.form.field.Checkbox', { fieldLabel: 'Is Account Detail', name: 'IsAccountDetail', anchor: '98%', inputValue: true, uncheckedValue: false, checked: true, hidden: true });
		me.AccountCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Account Code', allowBlank: false, name: 'AccountCode', anchor: '96%' });
		me.AccountName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Account Name', allowBlank: false, name: 'AccountName', anchor: '96%' });
        me.AccountLevel = Ext.create('Ext.form.field.Number', { fieldLabel: 'Account Level', allowBlank: false, name: 'AccountLevel', hideTrigger: true, anchor: '96%' });

        //me.DefaultValue = Ext.create('Ext.form.field.Number', { fieldLabel: 'DefaultValue', allowBlank: true, name: 'DefaultValue', hideTrigger: true, anchor: '96%' });
        me.Debit = Ext.create('Ext.form.field.Radio', {
            boxLabel: 'Debit',
            width: '25%',
            name: 'DefaultValue',
            anchor: '96%',
            inputValue: 0,
            checked: true
        });
        me.Credit = Ext.create('Ext.form.field.Radio', {
            boxLabel: 'Credit',
            width: '25%',
            name: 'DefaultValue',
            anchor: '96%',
            inputValue: 1,
            checked: false
        });

		me.Notes = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 512, allowBlank: true, name: 'Notes', anchor: '98%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, me.CategoryAccountId, me.AccountTypeId, me.SchemaCode, me.ParentId, me.IsAccountDetail, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Chart Of Account"
            }, {
                xtype: 'fieldset',
                title: 'Information Chart Of Account',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.TreeAccount]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.AccountCategory, me.AccountType, me.AccountLevel, me.AccountCode, me.AccountName, {
                            xtype: 'fieldcontainer',
                            fieldLabel: 'Standar Post',
                            layout: 'hbox',
                            items: [{
                                xtype: 'container',
                                flex: 1,
                                border: false,
                                layout: 'anchor',
                                defaultType: 'textfield',
                                items: [me.Debit]
                            }, {
                                xtype: 'container',
                                flex: 1,
                                layout: 'anchor',
                                defaultType: 'textfield',
                                items: [me.Credit]
                            }]
                        }, me.Notes]
                    }]
                }]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
        me.TreeAccount.on('select', me.onSelectTreeAccount, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/chartofaccount/submit';
        me.onRefreskClicked();
    },

    onSelectTreeAccount: function (obj, record) {
        var me = this;
        me.ParentId.setValue(record.data.Id);
        me.CategoryAccountId.setValue(record.data.CategoryAccountId);
        me.AccountCategory.setValue(record.data.AccountCategory);
        me.AccountTypeId.setValue(record.data.AccountTypeId);
        me.AccountType.setValue(record.data.AccountType);
        me.AccountLevel.setValue(record.data.Level + 1);
    },
    onRefreskClicked: function () {
        var me = this;
        try {
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/chartofaccount/listtree',
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                params: { params: 'null' },
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        if (jresult.ListChartOfAccountTree == null || jresult.ListChartOfAccountTree.length === 0)
                            return;
                        me.TreeAccount.getRootNode().appendChild(jresult.ListChartOfAccountTree);
                        me.TreeAccount.expandAll();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (error) {
            k.msg.error(error);
        }
    },
    
    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input Chart Of Account');
        me.cancelmsg = 'Cancel Input Chart Of Account ?';

        me.confirm = 'Save Data Chart Of Account ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Chart Of Account ?';
        me.setTitle('Update Chart Of Account');

        me.confirm = 'Update Data Chart Of Account ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close Chart Of Account ?';
        me.setTitle('Chart Of Account');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.SchemaCode.setReadOnly(true); 
		me.ParentId.setReadOnly(true); 
		me.IsAccountDetail.setReadOnly(true); 
		me.AccountTypeId.setReadOnly(true); 
		me.AccountCode.setReadOnly(true); 
		me.AccountName.setReadOnly(true); 
		me.AccountLevel.setReadOnly(true); 
		me.DefaultValue.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
		me.CategoryAccountId.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/chartofaccount/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.ChartOfAccount);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


