Ext.define('app.Accounting.Master.CustomerShipment.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 500,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Customer Shipment',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();

        me.IsCloseForm = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'Tutup Setelah Penyimpanan Data'
        });
        me.IsResetForm = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'Clear Form Setelah Penyimpanan Data'
        });
        me.ToolbarTop = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'top',
            items: [me.IsCloseForm, { xtype: 'tbseparator' }, me.IsResetForm]
        });
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        me.CustomerId = Ext.create('Ext.form.field.Hidden', { name: 'CustomerId' });

        me.IsDefaultShipment = Ext.create('Ext.form.field.Checkbox', { fieldLabel: 'Is Default Shipment', name: 'IsDefaultShipment', anchor: '98%', inputValue: true, uncheckedValue: false, checked: true });
		me.Carrier = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Carrier', allowBlank: true, name: 'Carrier', anchor: '96%' });
		me.Consignee = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Consignee', allowBlank: true, name: 'Consignee', anchor: '96%' });
        me.PortOfOrigin = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Port Of Origin', allowBlank: true, name: 'PortOfOrigin', anchor: '96%' });
        me.PortOfDestination = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Port Of Destination', allowBlank: true, name: 'PortOfDestination', anchor: '96%' });
		me.Country = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Country', allowBlank: false, name: 'Country', anchor: '96%' });
		me.Province = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Province', allowBlank: false, name: 'Province', anchor: '96%' });
		me.City = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'City', allowBlank: false, name: 'City', anchor: '96%' });
        me.Address = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Address', allowBlank: false, name: 'Address', anchor: '96%' });
		me.ZipCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Zip Code', allowBlank: false, name: 'ZipCode', anchor: '96%' });
		me.Notes = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 512, allowBlank: true, name: 'Notes', anchor: '98%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            dockedItems: [me.ToolbarTop],
            items: [me.objId, me.User, me.CustomerId,  /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Customer Shipment"
            }, {
                xtype: 'fieldset',
                title: 'Information Shipment',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.IsDefaultShipment, {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.Carrier, me.PortOfOrigin, me.Address, me.Country]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.Consignee, me.PortOfDestination, me.City, me.Province, me.ZipCode]
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Catatan',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.Notes]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/customershipment/submit';
    },
    
    create: function (id) {
        var me = this;
        me.show();
        me.setTitle('Input Customer Shipment');
        me.cancelmsg = 'Cancel Input Customer Shipment ?';

        me.confirm = 'Save Data Customer Shipment ?';
        me.CustomerId.setValue(id);
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Customer Shipment ?';
        me.setTitle('Update Customer Shipment');

        me.confirm = 'Update Data Customer Shipment ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close Customer Shipment ?';
        me.setTitle('Customer Shipment');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.CustomerId.setReadOnly(true); 
		me.IsDefaultShipment.setReadOnly(true); 
		me.Carrier.setReadOnly(true); 
		me.Consignee.setReadOnly(true); 
		me.PortOfOrigin.setReadOnly(true); 
		me.PortOfDestination.setReadOnly(true); 
		me.Country.setReadOnly(true); 
		me.Province.setReadOnly(true); 
		me.City.setReadOnly(true); 
		me.Address.setReadOnly(true); 
		me.ZipCode.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
    },
    onResetForm: function () {
        var me = this;
        me.objId.setValue('');
        //me.RowStatus.setValue('');
        //me.RowStatus.setValue('');
        //me.CustomerId.setValue(''); 
        me.IsDefaultShipment.setValue(false);
        me.Carrier.setValue('');
        me.Consignee.setValue('');
        me.PortOfOrigin.setValue('');
        me.PortOfDestination.setValue('');
        me.Address.setValue('');
        me.City.setValue('');
        me.Province.setValue('');
        me.Country.setValue('');
        me.ZipCode.setValue('');
        me.Notes.setValue(''); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                var resetForm = me.IsResetForm.getValue(), isCloseForm = me.IsCloseForm.getValue();
                                if (resetForm === true && isCloseForm === false) {
                                    me.onResetForm();
                                } else if (resetForm === false && isCloseForm === false) {
                                    if (jresult.CustomerShipment)
                                        me.cpanel.getForm().setValues(jresult.CustomerShipment);

                                    me.cancelmsg = 'Batalkan Update Customer Shipment ?';
                                    me.setTitle('Update Customer Shipment');

                                } else {
                                    // ReSharper disable once Html.EventNotResolved
                                    me.fireEvent("afterInsertSuccessEvent");
                                    me.close();
                                }
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/customershipment/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.CustomerShipment);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


