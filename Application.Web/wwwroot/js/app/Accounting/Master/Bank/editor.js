Ext.define('app.Accounting.Master.Bank.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 470,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Bank',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        
		me.BankCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Bank Code', allowBlank: false, name: 'BankCode', anchor: '96%' });
		me.BankName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Bank Name', allowBlank: false, name: 'BankName', anchor: '96%' });
		me.Country = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Country', allowBlank: false, name: 'Country', anchor: '96%' });
		me.Province = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Province', allowBlank: false, name: 'Province', anchor: '96%' });
		me.City = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'City', allowBlank: false, name: 'City', anchor: '96%' });
		me.Address = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Address', allowBlank: false, name: 'Address', anchor: '98%' });
		me.ZipCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Zip Code', allowBlank: false, name: 'ZipCode', anchor: '96%' });
		me.Phone = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Phone', allowBlank: false, name: 'Phone', anchor: '96%' });
		me.Phone2 = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Phone2', allowBlank: true, name: 'Phone2', anchor: '96%' });
		me.Fax = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Fax', allowBlank: true, name: 'Fax', anchor: '96%' });
		me.Email = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Email', allowBlank: false, name: 'Email', anchor: '96%' });
		me.WebAddress = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Web Address', allowBlank: true, name: 'WebAddress', anchor: '98%' });
		me.ContactPersonName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Contact Person Name', allowBlank: true, name: 'ContactPersonName', anchor: '96%' });
		me.ContactPersonPhone = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Contact Person Phone', allowBlank: true, name: 'ContactPersonPhone', anchor: '96%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Bank"
            }, {
                xtype: 'fieldset',
                title: 'Bank Information',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.BankCode, me.ContactPersonName]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                            items: [me.BankName, me.ContactPersonPhone]
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Address Information',
                defaultType: 'textfield',
                items: [me.Address, {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.City, me.Country, me.Phone, me.Email]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.Province, me.ZipCode, me.Phone2, me.Fax]
                    }]
                }, me.WebAddress]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/bank/submit';
    },

    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input Bank');
        me.cancelmsg = 'Cancel Input Bank ?';

        me.confirm = 'Save Data Bank ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Bank ?';
        me.setTitle('Update Bank');

        me.confirm = 'Update Data Bank ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close Bank ?';
        me.setTitle('Bank');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.BankCode.setReadOnly(true); 
		me.BankName.setReadOnly(true); 
		me.Country.setReadOnly(true); 
		me.Province.setReadOnly(true); 
		me.City.setReadOnly(true); 
		me.Address.setReadOnly(true); 
		me.ZipCode.setReadOnly(true); 
		me.Phone.setReadOnly(true); 
		me.Phone2.setReadOnly(true); 
		me.Fax.setReadOnly(true); 
		me.Email.setReadOnly(true); 
		me.WebAddress.setReadOnly(true); 
		me.ContactPersonName.setReadOnly(true); 
		me.ContactPersonPhone.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/bank/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.Bank);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


