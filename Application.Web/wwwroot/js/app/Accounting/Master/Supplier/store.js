Ext.define('app.Accounting.Master.Supplier.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'CategorySupplierId',
        'CategorySupplierName',
		'SupplierCode',
		'SupplierName',
		'Country',
		'Province',
		'City',
		'Address',
		'ZipCode',
		'Phone',
		'Phone2',
		'Fax',
		'Email',
		'WebAddress',
		'ContactPersonName',
		'ContactPersonPhone',
		'DefaultCurrency',
		'DefaultPaymentType',
		'DefaultVAT',
		'DefaultOtherTax',
		'DefaultOtherTaxRemark',
		'DefaultFreightCost',
		'DefaultOtherCost',
		'DefaultOtherCostRemark',
		'IsActive',
		'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/supplier/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListSupplier',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

