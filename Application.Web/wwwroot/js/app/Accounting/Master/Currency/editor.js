Ext.define('app.Accounting.Master.Currency.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 400,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Currency',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        
		me.CurrencyCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Currency Code', allowBlank: false, name: 'CurrencyCode', anchor: '98%' });
		me.CurrencyName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Currency Name', allowBlank: false, name: 'CurrencyName', anchor: '98%' });
		me.ExchangeDate = Ext.create('Ext.form.field.Date', { fieldLabel: 'Exchange Date', format: k.format.date, allowBlank: false, name: 'ExchangeDate', anchor: '98%' });
		me.BaseValue = Ext.create('Ext.form.field.Number', { fieldLabel: 'Base Value', allowBlank: false, name: 'BaseValue',hideTrigger: true, anchor: '98%', value: 0 });
		me.BaseRate = Ext.create('Ext.form.field.Number', { fieldLabel: 'Base Rate', allowBlank: false, name: 'BaseRate',hideTrigger: true, anchor: '98%', value: 0 });
		me.Notes = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 512, fieldLabel: 'Notes', allowBlank: true, name: 'Notes', anchor: '98%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Currency"
            }, {
                xtype: 'fieldset',
                title: 'Currency Information',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.CurrencyCode, me.CurrencyName, me.ExchangeDate, me.BaseValue, me.BaseRate, me.Notes, ]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/currency/submit';
    },

    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input Currency');
        me.cancelmsg = 'Cancel Input Currency ?';

        me.confirm = 'Save Data Currency ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Currency ?';
        me.setTitle('Update Currency');

        me.confirm = 'Update Data Currency ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close Currency ?';
        me.setTitle('Currency');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.CurrencyCode.setReadOnly(true); 
		me.CurrencyName.setReadOnly(true); 
		me.ExchangeDate.setReadOnly(true); 
		me.BaseValue.setReadOnly(true); 
		me.BaseRate.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/currency/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.Currency);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


