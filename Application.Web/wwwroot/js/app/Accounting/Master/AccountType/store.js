Ext.define('app.Accounting.Master.AccountType.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true,
    fields: [
        'RecordStatus',
        'Id',
        { name: 'RowStatus', type: 'int' },
        'RowVersion',
        'CreatedBy',
        { name: 'CreatedDate', type: 'date' },
        'ModifiedBy',
        { name: 'ModifiedDate', type: 'date' },
        'AccountTypeCode',
        'AccountTypeName',
        'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/accounttype/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListAccountType',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

