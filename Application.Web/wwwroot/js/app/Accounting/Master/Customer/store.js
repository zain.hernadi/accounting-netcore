Ext.define('app.Accounting.Master.Customer.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true,
    fields: [
        'RecordStatus',
        'Id',
        { name: 'RowStatus', type: 'int' },
        'RowVersion',
        'CreatedBy',
        { name: 'CreatedDate', type: 'date' },
        'ModifiedBy',
        { name: 'ModifiedDate', type: 'date' },
        'CategoryCustomerId',
        'CategoryName',
        'CustomerCode',
        'CustomerName',
        { name: 'MinimumDownPayment', type: 'float' },
        'Country',
        'Province',
        'City',
        'Address',
        'ZipCode',
        'Phone',
        'Phone2',
        'Fax',
        'Email',
        'WebAddress',
        'ContactPersonName',
        'ContactPersonPhone',
        'IsActive',
        'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/customer/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListCustomer',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

