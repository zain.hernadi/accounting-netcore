Ext.define('app.Accounting.Master.Customer.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 600,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Customer',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();

        me.buttonLink = k.btn.ok({ text: 'Input Shipment', iconCls: 'icon-template', scale: 'small' });
        me.IsCloseForm = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'Tutup Setelah Penyimpanan Data'
        });
        me.IsResetForm = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'Clear Form Setelah Penyimpanan Data'
        });
        me.ToolbarTop = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'top',
            //hidden: true,
            items: [me.IsCloseForm, { xtype: 'tbseparator' }, me.IsResetForm, { xtype: 'tbseparator' }, me.buttonLink]
        });
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        me.CategoryCustomerId = Ext.create('Ext.form.field.Hidden', { name: 'CategoryCustomerId' });

        me.CategoryCustomerStore = Ext.create('app.Accounting.Master.CategoryCustomer.store');
        me.CategoryCustomerStore.on('beforeload', function (store) {
            store.getProxy().url = k.app.apiAccountingUrl + 'api/accounting/categorycustomer/list';
            store.getProxy().setExtraParam('params', 'null');
        }, me);
        me.CustomerCategory = Ext.create('Ext.form.field.ComboBox', {
            enforceMaxLength: true, maxLength: 64, fieldLabel: 'Customer Category', allowBlank: false, name: 'CategoryName', anchor: '96%', editable: false,
            queryMode: 'remote',
            typeAhead: false,
            hideTrigger: false,
            forceSelection: false,
            enableKeyEvents: true,
            valueField: 'CategoryName',
            displayField: 'CategoryName',
            store: me.CategoryCustomerStore,
            listeners: {
                change: function (ele, newValue, oldValue) {
                    if (newValue !== oldValue && newValue !== ele.lastSelectEvent) {
                        this.store.data.items.forEach(function (e) {
                            if (e.data.CategoryName === newValue) {
                                dum = e.data.Id;
                                return;
                            }
                        });
                        me.CategoryCustomerId.setValue(dum);
                    }
                }
            }
            //value: 'REGULAR'
        });

		me.CustomerCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Customer Code', allowBlank: false, name: 'CustomerCode', anchor: '96%' });
		me.CustomerName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Customer Name', allowBlank: false, name: 'CustomerName', anchor: '96%' });
        me.MinimumDownPayment = Ext.create('Ext.form.field.Number', { fieldLabel: 'Minimum Down Payment (%)', allowBlank: false, name: 'MinimumDownPayment',hideTrigger: true, anchor: '96%', value: 0 });
		me.Country = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Country', allowBlank: false, name: 'Country', anchor: '96%' });
		me.Province = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Province', allowBlank: false, name: 'Province', anchor: '96%' });
		me.City = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'City', allowBlank: false, name: 'City', anchor: '96%' });
		me.Address = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Address', allowBlank: false, name: 'Address', anchor: '96%' });
		me.ZipCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Zip Code', allowBlank: false, name: 'ZipCode', anchor: '96%' });
		me.Phone = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Phone 1', allowBlank: false, name: 'Phone', anchor: '96%' });
		me.Phone2 = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Phone 2', allowBlank: true, name: 'Phone2', anchor: '96%' });
		me.Fax = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Fax', allowBlank: true, name: 'Fax', anchor: '96%' });
		me.Email = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Email', allowBlank: false, name: 'Email', anchor: '96%' });
		me.WebAddress = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Web Address', allowBlank: true, name: 'WebAddress', anchor: '96%' });
		me.ContactPersonName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Contact Person Name', allowBlank: true, name: 'ContactPersonName', anchor: '96%' });
		me.ContactPersonPhone = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Contact Person Phone', allowBlank: true, name: 'ContactPersonPhone', anchor: '96%' });
        me.IsActive = Ext.create('Ext.form.field.Checkbox', { fieldLabel: 'Is Active', name: 'IsActive', anchor: '98%', inputValue: true, uncheckedValue: false, checked: true });
		me.Notes = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 512, allowBlank: true, name: 'Notes', anchor: '98%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            dockedItems: [me.ToolbarTop],
            items: [me.objId, me.User, me.CategoryCustomerId, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Customer"
            }, {
                xtype: 'fieldset',
                title: 'Information Customer',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.CustomerCode, me.CustomerCategory, me.ContactPersonName, me.IsActive]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.CustomerName, me.MinimumDownPayment, me.ContactPersonPhone]
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Information Contact Customer',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.Phone, me.Fax, me.ZipCode, me.City, me.Country, me.Email]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.Phone2, me.Address, me.Province, me.ContactPerson, me.WebAddress]
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Catatan',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.Notes]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
        me.buttonLink.on('click', me.onButtonSetShipment, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onButtonSetShipment: function () {
        var me = this,
            id = me.objId.getValue();
        if (id === null || id === '') {
            k.msg.info('Data Customer Belum Tersimpan!');
            return;
        }
        me.editor = Ext.create('app.Accounting.Master.CustomerShipment.editor');
        me.editor.create(id);
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/customer/submit';
    },

    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input Customer');
        me.cancelmsg = 'Cancel Input Customer ?';

        me.confirm = 'Save Data Customer ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Customer ?';
        me.setTitle('Update Customer');

        me.confirm = 'Update Data Customer ?';
        me.buttonOk.setText('Update');
        me.CustomerCode.setReadOnly(true); 
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close Customer ?';
        me.setTitle('Customer');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.CategoryCustomerId.setReadOnly(true); 
        me.CustomerCategory.setReadOnly(true); 
        me.CustomerCode.setReadOnly(true); 
		me.CustomerName.setReadOnly(true); 
		me.MinimumDownPayment.setReadOnly(true); 
		me.Country.setReadOnly(true); 
		me.Province.setReadOnly(true); 
		me.City.setReadOnly(true); 
		me.Address.setReadOnly(true); 
		me.ZipCode.setReadOnly(true); 
		me.Phone.setReadOnly(true); 
		me.Phone2.setReadOnly(true); 
		me.Fax.setReadOnly(true); 
		me.Email.setReadOnly(true); 
		me.WebAddress.setReadOnly(true); 
		me.ContactPersonName.setReadOnly(true); 
		me.ContactPersonPhone.setReadOnly(true); 
		me.IsActive.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
    },
    onResetForm: function () {
        var me = this;
        me.objId.setValue('');
        me.CategoryCustomerId.setValue('');
        me.CustomerCategory.setValue('');
        me.CustomerCode.setValue('');
        me.CustomerName.setValue('');
        me.MinimumDownPayment.setValue(0);
        me.Country.setValue('');
        me.Province.setValue('');
        me.City.setValue('');
        me.Address.setValue('');
        me.ZipCode.setValue('');
        me.Phone.setValue('');
        me.Phone2.setValue('');
        me.Fax.setValue('');
        me.Email.setValue('');
        me.WebAddress.setValue('');
        me.ContactPersonName.setValue('');
        me.ContactPersonPhone.setValue('');
        me.IsActive.checked = true;
        me.Notes.setValue('');
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                var resetForm = me.IsResetForm.getValue(), dontCloseForm = me.IsCloseForm.getValue();
                                if (resetForm === true && dontCloseForm === false) {
                                    me.onResetForm();
                                } else if (resetForm === false && dontCloseForm === false) {
                                    if (jresult.Customer)
                                        me.cpanel.getForm().setValues(jresult.Customer);

                                    me.cancelmsg = 'Batalkan Update Customer ?';
                                    me.setTitle('Update Customer');

                                } else {
                                    me.fireEvent("afterInsertSuccessEvent");
                                    me.close();
                                }
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/customer/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.Customer);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


