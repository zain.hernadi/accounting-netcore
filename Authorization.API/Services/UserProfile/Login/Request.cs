﻿using MediatR;
using Authorization.API.Common.API;

namespace Authorization.API.Services.UserProfile.Login
{
    public class Request : IRequest<ApiResult<Response>>
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
