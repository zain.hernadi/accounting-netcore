﻿using MediatR;
using Newtonsoft.Json;
using Authorization.API.Common.API;

namespace Authorization.API.Services.UserProfile.Get
{
    public class Request : IRequest<ApiResult<Response>>
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
    }
}
