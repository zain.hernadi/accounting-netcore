﻿using Newtonsoft.Json;
using Authorization.API.Common.API;

namespace Authorization.API.Services.UserProfile.Get
{
    public class Response : BaseResponse
    {
        [JsonProperty(PropertyName = "UserProfile")]
        public Context.UserProfile UserProfileModel { get; set; }
    }
}
