﻿using Authorization.API.Common.API;
using MediatR;

namespace Authorization.API.Services.UserProfile.ListPaging
{
    public class Request : BaseListRequest, IRequest<ApiResult<Response>>
    {

    }
}
