﻿using Authorization.API.Common.API;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Authorization.API.Services.UserProfile.ListPaging
{
    public class Response : BaseResponse
    {
        [JsonProperty(PropertyName = "ListUserProfile")]
        public List<Context.UserProfile> ListUserProfile { get; set; }
    }
}
