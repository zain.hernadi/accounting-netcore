﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Authorization.API.Common.API;
using Authorization.API.Context;

namespace Authorization.API.Services.UserProfile.Delete
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly DataContext _dbContext;
        public Handler(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _dbContext.Database.BeginTransaction())
                {

                    var userProfile = await _dbContext.UserProfile.FirstOrDefaultAsync(c => c.Id == new Guid(request.Id));
                    if (userProfile == null)
                    {
                        return ApiResult<Response>.NotFound("Data User Profile not found!");
                    }
                    userProfile.RowStatus = 1;
                    userProfile.ModifiedBy = request.User;
                    userProfile.ModifiedDate = DateTime.Now;
                    _dbContext.UserProfile.Update(userProfile);

                    //_dbContext.Remove(employee);
                    await _dbContext.SaveChangesAsync();
                    scope.Commit();

                    return ApiResult<Response>.Ok(new Response
                    {
                        msg = "Delete User Profile Success",
                        success = true,
                        result = true,
                    });
                }
            }
            catch (Exception ex)
            {
                return ApiResult<Response>.ErrorDuringProcessing(ex.Message);
            }
        }
    }
}
