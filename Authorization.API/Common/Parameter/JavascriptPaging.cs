﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization.API.Common.Parameter
{
    [Serializable]
    public class JavascriptPaging
    {
        public string Kolom { get; set; }
        public string KolomDisplay { get; set; }
        public string Sorting { get; set; }
        public string SortingDisplay { get; set; }

        public string GetSortForLinq()
        {
            return Sorting == "0" ? "ASC" : "DESC";
        }
    }
}
