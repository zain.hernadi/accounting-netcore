﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization.API.Common.Parameter
{
    public class JavascriptWhereTermList : IList<JavascriptWhereTerm>
    {
        private readonly IList<JavascriptWhereTerm> list;
        public JavascriptWhereTermList()
        {
            list = new List<JavascriptWhereTerm>();
        }

        public JavascriptWhereTermList(string jparams)
        {
            list = new List<JavascriptWhereTerm>();
            var javascriptWhereTerms = JsonConvert.DeserializeObject<List<JavascriptWhereTerm>>(jparams);
            if (javascriptWhereTerms == null) return;
            foreach (var javascriptWhereTerm in javascriptWhereTerms)
            {
                list.Add(javascriptWhereTerm);
            }
        }

        public List<IListParameter> GetWhereTerms()
        {
            return list.Select(j => j.GetWhereTerm()).ToList();
        }

        public IEnumerator<JavascriptWhereTerm> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(JavascriptWhereTerm item)
        {
            list.Add(item);
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(JavascriptWhereTerm item)
        {
            return list.Contains(item);
        }

        public void CopyTo(JavascriptWhereTerm[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(JavascriptWhereTerm item)
        {
            return list.Remove(item);
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return list.IsReadOnly;
            }
        }

        public int IndexOf(JavascriptWhereTerm item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, JavascriptWhereTerm item)
        {
            list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }

        public JavascriptWhereTerm this[int index]
        {
            get
            {
                return list[index];
            }
            set
            {
                list[index] = value;
            }
        }

        public IList<JavascriptWhereTerm> ToList()
        {
            return list;
        }
    }
}
