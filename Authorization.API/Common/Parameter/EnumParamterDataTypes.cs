﻿namespace Authorization.API.Common.Parameter
{
    public enum EnumParamterDataTypes
    {
        Character,
        Number,
        NumberRange,
        DateTime,
        DateTimeRange,
        NullableDateTime,
        Bool,
        Guid,
        RowStatus,
        Column
    }
}
