﻿namespace Authorization.API.Common.Parameter
{
    public interface IListParameter
    {
        EnumParamterDataTypes ParamDataType { get; set; }
        object GetValue();
        string TableName { get; set; }
        string ColumnName { get; set; }
        SqlOperator Operator { get; set; }
        bool HasValue { get; }
        object Value { get; set; }
        LogicalOperator Logical { get; set; }
    }
}
