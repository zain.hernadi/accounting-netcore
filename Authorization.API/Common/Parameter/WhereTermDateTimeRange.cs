﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization.API.Common.Parameter
{
    public class WhereTermDateTimeRange : IListRangeParameter
    {
        public EnumParamterDataTypes ParamDataType { get; set; }
        public WhereTermDateTimeRange(DateTime? date1, DateTime? date2)
        {
            Value1 = date1;
            Value2 = date2;
            Logical = LogicalOperator.AND;
        }

        public WhereTermDateTimeRange()
        {
            Value1 = null;
            Value2 = null;
            Logical = LogicalOperator.AND;
        }

        public object GetValue()
        {
            return Value1;
        }

        public static WhereTermDateTimeRange Parameter(DateTime v1, DateTime v2, string column)
        {
            return new WhereTermDateTimeRange(v1, v2)
            {
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.DateTimeRange,
                Operator = SqlOperator.Equals,
                ColumnName = column,
                ColumnName2 = column
            };
        }

        public static WhereTermDateTimeRange Parameter(DateTime v1, DateTime v2, string column, LogicalOperator logical)
        {
            return new WhereTermDateTimeRange(v1, v2)
            {
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.DateTimeRange,
                Operator = SqlOperator.Equals,
                ColumnName = column,
                ColumnName2 = column,
                Logical = logical
            };
        }

        public static WhereTermDateTimeRange Parameter(DateTime? v1, DateTime? v2, string column, LogicalOperator logical)
        {
            return new WhereTermDateTimeRange(v1, v2)
            {
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.DateTimeRange,
                Operator = SqlOperator.Equals,
                ColumnName = column,
                ColumnName2 = column,
                Logical = logical
            };
        }

        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public SqlOperator Operator { get; set; }
        public bool HasValue
        {
            get { return Value1 != null; }
        }

        public object Value { get; set; }
        public LogicalOperator Logical { get; set; }

        public object GetValue2()
        {
            return Value2;
        }
        public DateTime? Value1 { get; set; }
        public DateTime? Value2 { get; set; }
        public string TableName2 { get; set; }
        public string ColumnName2 { get; set; }
        public bool HasValue2 { get { return Value2 != null; } }
    }
}
