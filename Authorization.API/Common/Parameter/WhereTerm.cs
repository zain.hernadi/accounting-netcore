﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization.API.Common.Parameter
{
    public class WhereTerm : IListParameter
    {
        public WhereTerm()
        {
            Operator = SqlOperator.Equals;
            ParamDataType = EnumParamterDataTypes.Character;
            Logical = LogicalOperator.AND;
        }

        /// <summary>
		/// Get value of whereterm
		/// </summary>
		/// <returns>object</returns>
		public object GetValue()
        {
            return Value;
        }

        /// <summary>
        /// Get & set name of table. 
        /// Notes: Must set this property if query using more than 1 table
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Get & set column name. Must set this property for where term sql 
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// Get & Set Data type of column
        /// </summary>
        public EnumParamterDataTypes ParamDataType { get; set; }

        /// <summary>
        /// Get & Set Sql operator 
        /// </summary>
        public SqlOperator Operator { get; set; }

        public LogicalOperator Logical { get; set; }

        public bool HasValue
        {
            get
            {
                return !string.IsNullOrEmpty(Value == null ? null : Value.ToString());
            }
        }

        /// <summary>
        /// Get & set value of where term
        /// </summary>
        public object Value { get; set; }

        public static WhereTerm Default(int value, string column, SqlOperator sqlOperator)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.Number,
                Operator = sqlOperator,
                ColumnName = column
            };
        }
        public static void AddDefaultParameter(ref IListParameter[] parameters)
        {
            if (parameters == null || parameters.Length == 0)
            {
                Array.Resize(ref parameters, 1);
                parameters[0] = Default(0, "RowStatus", SqlOperator.Equals);
            }
            else
            {
                var hasRowstatus = false;
                foreach (var listParameter in parameters.Where(listParameter => listParameter.ColumnName == "RowStatus"))
                {
                    hasRowstatus = true;
                }
                if (hasRowstatus) return;
                Array.Resize(ref parameters, parameters.Length + 1);
                parameters[parameters.Length - 1] = Default(0, "RowStatus", SqlOperator.Equals);
            }
        }

        public static WhereTerm Default(DateTime value, string column, SqlOperator sqlOperator)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.DateTime,
                Operator = sqlOperator,
                ColumnName = column
            };
        }

        public static WhereTerm Default(string value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = EnumParamterDataTypes.Character,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }

        public static WhereTerm Default(DateTime? value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.DateTime,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }

        public static WhereTerm Default(double? value, string column, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.Number,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }

        public static WhereTerm Default(Guid value, string kolom, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.Guid,
                Operator = sqlOperator,
                ColumnName = kolom,
                Logical = logical
            };
        }

        public static IListParameter Default(bool value, string kolom, SqlOperator sqlOperator, LogicalOperator logical)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.Bool,
                Operator = sqlOperator,
                ColumnName = kolom,
                Logical = logical
            };
        }

        public static WhereTerm Default(string value, string column, SqlOperator sqlOperator, LogicalOperator logical, EnumParamterDataTypes type)
        {
            return new WhereTerm
            {
                Value = value,
                TableName = string.Empty,
                ParamDataType = type,
                Operator = sqlOperator,
                ColumnName = column,
                Logical = logical
            };
        }
    }
}
