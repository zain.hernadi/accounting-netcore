﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization.API.Common.Parameter
{
    public class JavascriptWhereTerm
    {
        public string Kolom { get; set; }
        public string KolomDisplay { get; set; }
        public string DataType { get; set; }
        public string Operator { get; set; }
        public string OperatorDisplay { get; set; }
        public string Keyword1 { get; set; }
        public string Keyword2 { get; set; }
        public string StringKeyword { get; set; }
        public double? NumberKeyword1 { get; set; }
        public double? NumberKeyword2 { get; set; }
        public DateTime? DateKeyword1 { get; set; }
        public DateTime? DateKeyword2 { get; set; }
        public string LogicalOperator { get; set; }
        public string LogicalOperatorDisplay { get; set; }

        public EnumParamterDataTypes GetDataType()
        {
            switch (DataType.ToLower())
            {
                case "rowstatus": return EnumParamterDataTypes.RowStatus;
                case "double": return EnumParamterDataTypes.Number;
                case "datetime": return EnumParamterDataTypes.DateTime;
                case "guid": return EnumParamterDataTypes.Guid;
                case "bool": return EnumParamterDataTypes.Bool;
                case "column": return EnumParamterDataTypes.Column;
                default: return EnumParamterDataTypes.Character;
            }
        }

        public SqlOperator GetSqlOperator()
        {
            switch (Operator)
            {
                case "1":
                    return SqlOperator.NotEqual;
                case "2":
                    return SqlOperator.BeginWith;
                case "3":
                    return SqlOperator.EndWith;
                case "4":
                    return SqlOperator.Like;
                case "5":
                    return SqlOperator.GreatThan;
                case "6":
                    return SqlOperator.GreatThanEqual;
                case "7":
                    return SqlOperator.LessThan;
                case "8":
                    return SqlOperator.LesThanEqual;
                case "9":
                    return SqlOperator.Beetween;
                case "10":
                    return SqlOperator.IsNull;
                default:
                    return SqlOperator.Equals;
            }
        }

        public LogicalOperator GetLogicalOperator()
        {
            switch (LogicalOperator)
            {
                case "1": return Parameter.LogicalOperator.OR;
                default: return Parameter.LogicalOperator.AND;
            }

        }

        public IListParameter GetWhereTerm()
        {
            switch (GetSqlOperator())
            {
                case SqlOperator.Beetween:
                    if (GetDataType() == EnumParamterDataTypes.DateTime)
                    {
                        return WhereTermDateTimeRange.Parameter(DateKeyword1, DateKeyword2, Kolom, GetLogicalOperator());
                    }
                    if (GetDataType() == EnumParamterDataTypes.Number)
                    {
                        return WhereTermNumberRange.Parameter(NumberKeyword1, NumberKeyword2, Kolom, GetLogicalOperator());
                    }
                    return WhereTerm.Default(StringKeyword, Kolom, GetSqlOperator(), GetLogicalOperator());
                default:
                    switch (GetDataType())
                    {
                        case EnumParamterDataTypes.DateTime:
                            return WhereTerm.Default(DateKeyword1, Kolom, GetSqlOperator(), GetLogicalOperator());
                        case EnumParamterDataTypes.Number:
                            return WhereTerm.Default(NumberKeyword1, Kolom, GetSqlOperator(), GetLogicalOperator());
                        case EnumParamterDataTypes.RowStatus:
                            return WhereTerm.Default(NumberKeyword1, Kolom, GetSqlOperator(), GetLogicalOperator());
                        case EnumParamterDataTypes.Guid:
                            return WhereTerm.Default(new Guid(Keyword1), Kolom, GetSqlOperator(), GetLogicalOperator());
                        case EnumParamterDataTypes.Bool:
                            return WhereTerm.Default(Convert.ToBoolean(Keyword1), Kolom, GetSqlOperator(), GetLogicalOperator());
                        case EnumParamterDataTypes.Column:
                            return WhereTerm.Default(StringKeyword, Kolom, GetSqlOperator(), GetLogicalOperator(), EnumParamterDataTypes.Column);
                        default:
                            return WhereTerm.Default(StringKeyword, Kolom, GetSqlOperator(), GetLogicalOperator());
                    }
            }
        }
    }
}
