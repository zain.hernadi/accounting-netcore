﻿namespace Authorization.API.Common.Parameter
{
    public enum SqlOperator
    {
        Equals,
        NotEqual,
        GreatThan,
        GreatThanEqual,
        LessThan,
        LesThanEqual,
        BeginWith,
        EndWith,
        Like,
        Beetween,
        IsNull
    }
}
