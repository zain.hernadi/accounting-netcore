﻿using Authorization.API.Common.Manager;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Authorization.API.Common.Parameter
{
    public class ListParameter : IList<IListParameter>
    {
        private readonly IList<IListParameter> list;

        public ListParameter()
        {
            list = new List<IListParameter>();
        }

        public Tuple<string, Collection<object>> GetWhereTerm()
        {
            return Tuple.Create(BaseDataManager.GetQueryParameterLinqV02(list.ToArray()), BaseDataManager.ListValue);
        }

        public IEnumerator<IListParameter> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(IListParameter item)
        {
            list.Add(item);
        }

        public void Clear()
        {
            list.Clear();
        }

        public bool Contains(IListParameter item)
        {
            return list.Contains(item);
        }

        public void CopyTo(IListParameter[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public bool Remove(IListParameter item)
        {
            return list.Remove(item);
        }

        public int Count
        {
            get
            {
                return list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return list.IsReadOnly;
            }
        }

        public int IndexOf(IListParameter item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, IListParameter item)
        {
            list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            list.RemoveAt(index);
        }

        public IListParameter this[int index]
        {
            get
            {
                return list[index];
            }
            set
            {
                list[index] = value;
            }
        }

        public static ListParameter GetListParameter(string array)
        {
            var javascriptWhereTermList = new JavascriptWhereTermList(array);
            var where = new ListParameter();
            foreach (var ja in javascriptWhereTermList.ToList())
            {
                where.Add(ja.GetWhereTerm());
            }
            return where;
        }

        public IList<IListParameter> ToList()
        {
            return list.ToList();
        }
    }
}
