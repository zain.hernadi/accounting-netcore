﻿using Newtonsoft.Json;

namespace Authorization.API.Common.API
{
    public class BaseResponseEndPoint
    {
        [JsonProperty(PropertyName = "ErrorCode")]
        public int ErrorCode { get; set; }

        [JsonProperty(PropertyName = "ErrorDescription")]
        public string ErrorDescription { get; set; }

        [JsonProperty(PropertyName = "IsSuccessful")]
        public bool IsSuccessful { get; set; }
    }
}
