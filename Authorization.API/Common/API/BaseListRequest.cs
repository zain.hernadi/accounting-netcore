﻿using Newtonsoft.Json;

namespace Authorization.API.Common.API
{
    public class BaseListRequest
    {
        [JsonProperty(PropertyName = "_dc")]
        public long _DC { get; set; }

        [JsonProperty(PropertyName = "params")]
        public string Params { get; set; }

        [JsonProperty(PropertyName = "sorters")]
        public string Sorters { get; set; }

        [JsonProperty(PropertyName = "sort")]
        public string Sort { get; set; }

        [JsonProperty(PropertyName = "page")]
        public int Page { get; set; }

        [JsonProperty(PropertyName = "start")]
        public int Start { get; set; }

        [JsonProperty(PropertyName = "limit")]
        public int Limit { get; set; }
    }
}
