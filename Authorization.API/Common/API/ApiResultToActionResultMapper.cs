using Microsoft.AspNetCore.Mvc;

namespace Authorization.API.Common.API
{
    public static class ApiResultToActionResultMapper
    {
        public static IActionResult ToActionResult(ApiResult result)
        {
            return result.IsSuccessful
                ? new ObjectResult(null)
                    { StatusCode = (int)result.HttpStatusCode }

                : result.ErrorCode == 0
                    ? new ObjectResult(new Error { ErrorDescription = result.ErrorDescription })
                        { StatusCode = result.HttpStatusCode }
                    : new ObjectResult(new ErrorWithErrorCode { ErrorDescription = result.ErrorDescription, ErrorCode = result.ErrorCode })
                        { StatusCode = result.HttpStatusCode };
        }

        public static IActionResult ToActionResult<TResponse>(ApiResult<TResponse> result)
        {
            return result.IsSuccessful
                ? new ObjectResult(result.Value)
                    { StatusCode = (int)result.HttpStatusCode }

                : result.ErrorCode == 0
                    ? new ObjectResult(new Error { ErrorDescription = result.ErrorDescription })
                        { StatusCode = (int)result.HttpStatusCode }
                    : new ObjectResult(new ErrorWithErrorCode() { ErrorDescription = result.ErrorDescription, ErrorCode = result.ErrorCode })
                        { StatusCode = (int)result.HttpStatusCode };
        }


        public static IActionResult ToActionResult<T>(T result)
        {
            throw new System.NotImplementedException();
        }
    }
    
    public class Error
    {
        public string ErrorDescription { get; set; }
    }

    public class ErrorWithErrorCode
    {
        public bool IsSuccessful { get; set; }
        public string ErrorDescription { get; set; }
        public int ErrorCode { get; set; }
    }
}