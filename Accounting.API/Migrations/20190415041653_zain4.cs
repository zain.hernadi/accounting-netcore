﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounting.API.Migrations
{
    public partial class zain4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChartOfAccounts_AccountCategories_AccountCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.DropTable(
                name: "AccountCategories");

            migrationBuilder.DropIndex(
                name: "IX_ChartOfAccounts_AccountCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.AddColumn<Guid>(
                name: "CategoryAccountId",
                table: "ChartOfAccounts",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CategoryAccounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    AccountCategoryCode = table.Column<string>(maxLength: 24, nullable: false),
                    AccountCategoryName = table.Column<string>(maxLength: 64, nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryAccounts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChartOfAccounts_CategoryAccountId",
                table: "ChartOfAccounts",
                column: "CategoryAccountId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryAccounts_AccountCategoryCode",
                table: "CategoryAccounts",
                column: "AccountCategoryCode",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ChartOfAccounts_CategoryAccounts_CategoryAccountId",
                table: "ChartOfAccounts",
                column: "CategoryAccountId",
                principalTable: "CategoryAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChartOfAccounts_CategoryAccounts_CategoryAccountId",
                table: "ChartOfAccounts");

            migrationBuilder.DropTable(
                name: "CategoryAccounts");

            migrationBuilder.DropIndex(
                name: "IX_ChartOfAccounts_CategoryAccountId",
                table: "ChartOfAccounts");

            migrationBuilder.DropColumn(
                name: "CategoryAccountId",
                table: "ChartOfAccounts");

            migrationBuilder.CreateTable(
                name: "AccountCategories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    AccountCategoryCode = table.Column<string>(maxLength: 24, nullable: false),
                    AccountCategoryName = table.Column<string>(maxLength: 64, nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Notes = table.Column<string>(maxLength: 512, nullable: true),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountCategories", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChartOfAccounts_AccountCategoryId",
                table: "ChartOfAccounts",
                column: "AccountCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountCategories_AccountCategoryCode",
                table: "AccountCategories",
                column: "AccountCategoryCode",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ChartOfAccounts_AccountCategories_AccountCategoryId",
                table: "ChartOfAccounts",
                column: "AccountCategoryId",
                principalTable: "AccountCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
