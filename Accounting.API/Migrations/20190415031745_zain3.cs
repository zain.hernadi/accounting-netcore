﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounting.API.Migrations
{
    public partial class zain3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Banks",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    BankCode = table.Column<string>(maxLength: 24, nullable: false),
                    BankName = table.Column<string>(maxLength: 64, nullable: false),
                    Country = table.Column<string>(maxLength: 128, nullable: false),
                    Province = table.Column<string>(maxLength: 128, nullable: false),
                    City = table.Column<string>(maxLength: 128, nullable: false),
                    Address = table.Column<string>(maxLength: 256, nullable: false),
                    ZipCode = table.Column<string>(maxLength: 24, nullable: false),
                    Phone = table.Column<string>(maxLength: 24, nullable: false),
                    Phone2 = table.Column<string>(maxLength: 24, nullable: true),
                    Fax = table.Column<string>(maxLength: 64, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    WebAddress = table.Column<string>(maxLength: 256, nullable: true),
                    ContactPersonName = table.Column<string>(maxLength: 64, nullable: true),
                    ContactPersonPhone = table.Column<string>(maxLength: 24, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Banks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CashPayments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    StatusApproval = table.Column<string>(maxLength: 24, nullable: true),
                    EmployeeCode = table.Column<string>(maxLength: 24, nullable: false),
                    EmployeeName = table.Column<string>(maxLength: 64, nullable: false),
                    DivisionName = table.Column<string>(maxLength: 64, nullable: false),
                    Position = table.Column<string>(maxLength: 64, nullable: false),
                    Location = table.Column<string>(maxLength: 64, nullable: false),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    CcEmail = table.Column<string>(maxLength: 256, nullable: false),
                    CashPaymentNo = table.Column<string>(maxLength: 24, nullable: false),
                    CashPaymentDate = table.Column<DateTime>(nullable: false),
                    CashPaymentStatus = table.Column<string>(maxLength: 24, nullable: false),
                    PaymentDocumentNo = table.Column<string>(maxLength: 24, nullable: true),
                    PaymentDocumentType = table.Column<string>(maxLength: 24, nullable: true),
                    PaymentDocumentEffectiveDate = table.Column<DateTime>(nullable: true),
                    PaymentDocumentExpiredDate = table.Column<DateTime>(nullable: true),
                    PaymentDocumentRemark = table.Column<string>(maxLength: 128, nullable: true),
                    ReceiverType = table.Column<string>(maxLength: 24, nullable: false),
                    ReceiverCode = table.Column<string>(maxLength: 24, nullable: true),
                    ReceiverName = table.Column<string>(maxLength: 64, nullable: false),
                    BankCode = table.Column<string>(maxLength: 24, nullable: true),
                    BankName = table.Column<string>(maxLength: 64, nullable: true),
                    RefDocumentType = table.Column<string>(maxLength: 24, nullable: true),
                    RefDocumentNo = table.Column<string>(maxLength: 24, nullable: true),
                    RefDocumentDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(maxLength: 512, nullable: false),
                    CurrencyCode = table.Column<string>(maxLength: 24, nullable: false),
                    TotalPayment = table.Column<decimal>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashPayments", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CashReceipts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    StatusApproval = table.Column<string>(maxLength: 24, nullable: true),
                    EmployeeCode = table.Column<string>(maxLength: 24, nullable: false),
                    EmployeeName = table.Column<string>(maxLength: 64, nullable: false),
                    DivisionName = table.Column<string>(maxLength: 64, nullable: false),
                    Position = table.Column<string>(maxLength: 64, nullable: false),
                    Location = table.Column<string>(maxLength: 64, nullable: false),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    CcEmail = table.Column<string>(maxLength: 256, nullable: false),
                    CashReceiptNo = table.Column<string>(maxLength: 24, nullable: false),
                    CashReceiptDate = table.Column<DateTime>(nullable: false),
                    CashReceiptStatus = table.Column<string>(maxLength: 24, nullable: false),
                    ReceiptDocumentNo = table.Column<string>(maxLength: 24, nullable: true),
                    ReceiptDocumentType = table.Column<string>(maxLength: 24, nullable: true),
                    ReceiptDocumentEffectiveDate = table.Column<DateTime>(nullable: true),
                    ReceiptDocumentExpiredDate = table.Column<DateTime>(nullable: true),
                    ReceiptDocumentRemark = table.Column<string>(maxLength: 128, nullable: true),
                    PayerType = table.Column<string>(maxLength: 24, nullable: false),
                    PayerCode = table.Column<string>(maxLength: 24, nullable: true),
                    PayerName = table.Column<string>(maxLength: 64, nullable: false),
                    BankCode = table.Column<string>(maxLength: 24, nullable: true),
                    BankName = table.Column<string>(maxLength: 64, nullable: true),
                    RefDocumentType = table.Column<string>(maxLength: 24, nullable: true),
                    RefDocumentNo = table.Column<string>(maxLength: 24, nullable: true),
                    RefDocumentDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(maxLength: 512, nullable: false),
                    CurrencyCode = table.Column<string>(maxLength: 24, nullable: false),
                    TotalPayment = table.Column<decimal>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashReceipts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryCustomers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CategoryName = table.Column<string>(maxLength: 24, nullable: false),
                    Priority = table.Column<byte>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryCustomers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategorySuppliers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CategoryName = table.Column<string>(maxLength: 24, nullable: false),
                    Priority = table.Column<byte>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategorySuppliers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Currencys",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CurrencyCode = table.Column<string>(maxLength: 24, nullable: false),
                    CurrencyName = table.Column<string>(maxLength: 64, nullable: false),
                    ExchangeDate = table.Column<DateTime>(nullable: false),
                    BaseValue = table.Column<decimal>(nullable: false),
                    BaseRate = table.Column<decimal>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Currencys", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FiscalYears",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    FiscalYearCode = table.Column<string>(maxLength: 24, nullable: false),
                    FiscalYearName = table.Column<string>(maxLength: 64, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false),
                    ClosingType = table.Column<byte>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FiscalYears", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GeneralLedgers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    StatusApproval = table.Column<string>(maxLength: 24, nullable: true),
                    EmployeeCode = table.Column<string>(maxLength: 24, nullable: false),
                    EmployeeName = table.Column<string>(maxLength: 64, nullable: false),
                    DivisionName = table.Column<string>(maxLength: 64, nullable: false),
                    Position = table.Column<string>(maxLength: 64, nullable: false),
                    Location = table.Column<string>(maxLength: 64, nullable: false),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    CcEmail = table.Column<string>(maxLength: 256, nullable: false),
                    FiscalYearCode = table.Column<string>(maxLength: 24, nullable: false),
                    GeneralLedgerNo = table.Column<string>(maxLength: 24, nullable: false),
                    GeneralLedgerName = table.Column<string>(maxLength: 128, nullable: false),
                    GeneralLedgerDate = table.Column<DateTime>(nullable: false),
                    GeneralLedgerType = table.Column<string>(maxLength: 64, nullable: false),
                    ReffDocumentCode = table.Column<string>(maxLength: 24, nullable: true),
                    ReffDocumentName = table.Column<string>(maxLength: 24, nullable: true),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(maxLength: 64, nullable: false),
                    CurrencyCode = table.Column<string>(maxLength: 24, nullable: false),
                    TotalDebet = table.Column<decimal>(nullable: false),
                    TotalCredit = table.Column<decimal>(nullable: false),
                    PostingDate = table.Column<DateTime>(nullable: false),
                    IsLastBalance = table.Column<bool>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralLedgers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportBalanceSheets",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    SessionId = table.Column<Guid>(nullable: false),
                    LineNumber = table.Column<int>(nullable: false),
                    AccountCode = table.Column<string>(maxLength: 24, nullable: false),
                    AccountName = table.Column<string>(maxLength: 64, nullable: false),
                    AccountLevel = table.Column<int>(nullable: false),
                    IsAccountDetail = table.Column<bool>(nullable: false),
                    Debet = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    Saldo = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportBalanceSheets", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportIncomeStatements",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    SessionId = table.Column<Guid>(nullable: false),
                    LineNumber = table.Column<int>(nullable: false),
                    LineLevel = table.Column<int>(nullable: false),
                    Description = table.Column<string>(maxLength: 128, nullable: false),
                    IsDetail = table.Column<bool>(nullable: false),
                    Saldo = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportIncomeStatements", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ReportPatterns",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    ReportName = table.Column<string>(maxLength: 64, nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportPatterns", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubsidiaryAccountsPayables",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    SupplierCode = table.Column<string>(maxLength: 24, nullable: false),
                    SupplierName = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidiaryAccountsPayables", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SubsidiaryAccountsReceivables",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CustomerCode = table.Column<string>(maxLength: 24, nullable: false),
                    CustomerName = table.Column<string>(maxLength: 128, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidiaryAccountsReceivables", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BankAccounts",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    BankId = table.Column<Guid>(nullable: false),
                    AccountNumber = table.Column<string>(maxLength: 64, nullable: false),
                    AccountName = table.Column<string>(maxLength: 128, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankAccounts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BankAccounts_Banks_BankId",
                        column: x => x.BankId,
                        principalTable: "Banks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CategoryCustomerId = table.Column<Guid>(nullable: false),
                    CustomerCode = table.Column<string>(maxLength: 24, nullable: false),
                    CustomerName = table.Column<string>(maxLength: 128, nullable: false),
                    MinimumDownPayment = table.Column<double>(nullable: false),
                    Country = table.Column<string>(maxLength: 128, nullable: false),
                    Province = table.Column<string>(maxLength: 128, nullable: false),
                    City = table.Column<string>(maxLength: 128, nullable: false),
                    Address = table.Column<string>(maxLength: 256, nullable: false),
                    ZipCode = table.Column<string>(maxLength: 24, nullable: false),
                    Phone = table.Column<string>(maxLength: 24, nullable: false),
                    Phone2 = table.Column<string>(maxLength: 24, nullable: true),
                    Fax = table.Column<string>(maxLength: 64, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    WebAddress = table.Column<string>(maxLength: 256, nullable: true),
                    ContactPersonName = table.Column<string>(maxLength: 64, nullable: true),
                    ContactPersonPhone = table.Column<string>(maxLength: 24, nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_CategoryCustomers_CategoryCustomerId",
                        column: x => x.CategoryCustomerId,
                        principalTable: "CategoryCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Suppliers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CategorySupplierId = table.Column<Guid>(nullable: false),
                    SupplierCode = table.Column<string>(maxLength: 24, nullable: false),
                    SupplierName = table.Column<string>(maxLength: 128, nullable: false),
                    Country = table.Column<string>(maxLength: 128, nullable: false),
                    Province = table.Column<string>(maxLength: 128, nullable: false),
                    City = table.Column<string>(maxLength: 128, nullable: false),
                    Address = table.Column<string>(maxLength: 256, nullable: false),
                    ZipCode = table.Column<string>(maxLength: 24, nullable: false),
                    Phone = table.Column<string>(maxLength: 24, nullable: false),
                    Phone2 = table.Column<string>(maxLength: 24, nullable: true),
                    Fax = table.Column<string>(maxLength: 64, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: false),
                    WebAddress = table.Column<string>(maxLength: 256, nullable: true),
                    ContactPersonName = table.Column<string>(maxLength: 64, nullable: true),
                    ContactPersonPhone = table.Column<string>(maxLength: 24, nullable: true),
                    DefaultCurrency = table.Column<string>(maxLength: 24, nullable: false),
                    DefaultPaymentType = table.Column<string>(maxLength: 24, nullable: false),
                    DefaultVAT = table.Column<decimal>(nullable: false),
                    DefaultOtherTax = table.Column<decimal>(nullable: false),
                    DefaultOtherTaxRemark = table.Column<decimal>(maxLength: 128, nullable: false),
                    DefaultFreightCost = table.Column<decimal>(nullable: false),
                    DefaultOtherCost = table.Column<decimal>(nullable: false),
                    DefaultOtherCostRemark = table.Column<decimal>(maxLength: 128, nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Suppliers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Suppliers_CategorySuppliers_CategorySupplierId",
                        column: x => x.CategorySupplierId,
                        principalTable: "CategorySuppliers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GeneralLedgerDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    GeneralLedgerId = table.Column<Guid>(nullable: false),
                    AccountCode = table.Column<string>(maxLength: 24, nullable: false),
                    AccountName = table.Column<string>(maxLength: 64, nullable: false),
                    Debet = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    CurrencyCode = table.Column<string>(maxLength: 24, nullable: false),
                    ExchangeRate = table.Column<decimal>(nullable: false),
                    ExchangeValueDebet = table.Column<decimal>(nullable: false),
                    ExchangeValueCredit = table.Column<decimal>(nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralLedgerDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeneralLedgerDetails_GeneralLedgers_GeneralLedgerId",
                        column: x => x.GeneralLedgerId,
                        principalTable: "GeneralLedgers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ReportPatternDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    ReportPatternId = table.Column<Guid>(nullable: false),
                    LineNumber = table.Column<int>(nullable: false),
                    AccountCode = table.Column<string>(maxLength: 24, nullable: true),
                    AccountName = table.Column<string>(maxLength: 64, nullable: false),
                    AccountLevel = table.Column<int>(nullable: false),
                    IsAccountDetail = table.Column<bool>(nullable: false),
                    FunctionValue = table.Column<string>(maxLength: 64, nullable: false),
                    FunctionAccount = table.Column<string>(maxLength: 64, nullable: true),
                    FunctionFormula = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ReportPatternDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ReportPatternDetails_ReportPatterns_ReportPatternId",
                        column: x => x.ReportPatternId,
                        principalTable: "ReportPatterns",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubsidiaryAccountsPayableDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    SubsidiaryAccountsPayableId = table.Column<Guid>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    DocumentType = table.Column<string>(maxLength: 24, nullable: false),
                    DocumentNo = table.Column<string>(maxLength: 24, nullable: false),
                    Description = table.Column<string>(maxLength: 512, nullable: false),
                    Debet = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidiaryAccountsPayableDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubsidiaryAccountsPayableDetails_SubsidiaryAccountsPayables~",
                        column: x => x.SubsidiaryAccountsPayableId,
                        principalTable: "SubsidiaryAccountsPayables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SubsidiaryAccountsReceivableDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    SubsidiaryAccountsReceivableId = table.Column<Guid>(nullable: false),
                    TransactionDate = table.Column<DateTime>(nullable: false),
                    DocumentType = table.Column<string>(maxLength: 24, nullable: false),
                    DocumentNo = table.Column<string>(maxLength: 24, nullable: false),
                    Description = table.Column<string>(maxLength: 512, nullable: false),
                    Debet = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubsidiaryAccountsReceivableDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubsidiaryAccountsReceivableDetails_SubsidiaryAccountsRecei~",
                        column: x => x.SubsidiaryAccountsReceivableId,
                        principalTable: "SubsidiaryAccountsReceivables",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CustomerShipments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RowStatus = table.Column<byte>(nullable: false),
                    RowVersion = table.Column<byte[]>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 24, nullable: false),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    ModifiedBy = table.Column<string>(maxLength: 24, nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    CustomerId = table.Column<Guid>(nullable: false),
                    IsDefaultShipment = table.Column<bool>(nullable: false),
                    Carrier = table.Column<string>(maxLength: 256, nullable: true),
                    Consignee = table.Column<string>(maxLength: 256, nullable: true),
                    PortOfOrigin = table.Column<string>(maxLength: 256, nullable: true),
                    PortOfDestination = table.Column<string>(maxLength: 256, nullable: true),
                    Country = table.Column<string>(maxLength: 128, nullable: false),
                    Province = table.Column<string>(maxLength: 128, nullable: false),
                    City = table.Column<string>(maxLength: 128, nullable: false),
                    Address = table.Column<string>(maxLength: 256, nullable: false),
                    ZipCode = table.Column<string>(maxLength: 24, nullable: false),
                    Notes = table.Column<string>(maxLength: 512, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerShipments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CustomerShipments_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BankAccounts_AccountNumber",
                table: "BankAccounts",
                column: "AccountNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BankAccounts_BankId",
                table: "BankAccounts",
                column: "BankId");

            migrationBuilder.CreateIndex(
                name: "IX_Banks_BankCode",
                table: "Banks",
                column: "BankCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CashPayments_CashPaymentNo",
                table: "CashPayments",
                column: "CashPaymentNo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CashReceipts_CashReceiptNo",
                table: "CashReceipts",
                column: "CashReceiptNo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CategoryCustomers_CategoryName",
                table: "CategoryCustomers",
                column: "CategoryName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CategorySuppliers_CategoryName",
                table: "CategorySuppliers",
                column: "CategoryName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Currencys_CurrencyCode",
                table: "Currencys",
                column: "CurrencyCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CategoryCustomerId",
                table: "Customers",
                column: "CategoryCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_CustomerCode",
                table: "Customers",
                column: "CustomerCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CustomerShipments_CustomerId",
                table: "CustomerShipments",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FiscalYears_FiscalYearCode",
                table: "FiscalYears",
                column: "FiscalYearCode",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GeneralLedgerDetails_GeneralLedgerId",
                table: "GeneralLedgerDetails",
                column: "GeneralLedgerId");

            migrationBuilder.CreateIndex(
                name: "IX_GeneralLedgers_GeneralLedgerNo",
                table: "GeneralLedgers",
                column: "GeneralLedgerNo",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ReportPatternDetails_ReportPatternId",
                table: "ReportPatternDetails",
                column: "ReportPatternId");

            migrationBuilder.CreateIndex(
                name: "IX_ReportPatterns_ReportName",
                table: "ReportPatterns",
                column: "ReportName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SubsidiaryAccountsPayableDetails_SubsidiaryAccountsPayableId",
                table: "SubsidiaryAccountsPayableDetails",
                column: "SubsidiaryAccountsPayableId");

            migrationBuilder.CreateIndex(
                name: "IX_SubsidiaryAccountsReceivableDetails_SubsidiaryAccountsRecei~",
                table: "SubsidiaryAccountsReceivableDetails",
                column: "SubsidiaryAccountsReceivableId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_CategorySupplierId",
                table: "Suppliers",
                column: "CategorySupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_Suppliers_SupplierCode",
                table: "Suppliers",
                column: "SupplierCode",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankAccounts");

            migrationBuilder.DropTable(
                name: "CashPayments");

            migrationBuilder.DropTable(
                name: "CashReceipts");

            migrationBuilder.DropTable(
                name: "Currencys");

            migrationBuilder.DropTable(
                name: "CustomerShipments");

            migrationBuilder.DropTable(
                name: "FiscalYears");

            migrationBuilder.DropTable(
                name: "GeneralLedgerDetails");

            migrationBuilder.DropTable(
                name: "ReportBalanceSheets");

            migrationBuilder.DropTable(
                name: "ReportIncomeStatements");

            migrationBuilder.DropTable(
                name: "ReportPatternDetails");

            migrationBuilder.DropTable(
                name: "SubsidiaryAccountsPayableDetails");

            migrationBuilder.DropTable(
                name: "SubsidiaryAccountsReceivableDetails");

            migrationBuilder.DropTable(
                name: "Suppliers");

            migrationBuilder.DropTable(
                name: "Banks");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "GeneralLedgers");

            migrationBuilder.DropTable(
                name: "ReportPatterns");

            migrationBuilder.DropTable(
                name: "SubsidiaryAccountsPayables");

            migrationBuilder.DropTable(
                name: "SubsidiaryAccountsReceivables");

            migrationBuilder.DropTable(
                name: "CategorySuppliers");

            migrationBuilder.DropTable(
                name: "CategoryCustomers");
        }
    }
}
