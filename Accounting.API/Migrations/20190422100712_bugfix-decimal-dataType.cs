﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounting.API.Migrations
{
    public partial class bugfixdecimaldataType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultVAT",
                table: "Suppliers",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "DefaultOtherTaxRemark",
                table: "Suppliers",
                maxLength: 128,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultOtherTax",
                table: "Suppliers",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "DefaultOtherCostRemark",
                table: "Suppliers",
                maxLength: 128,
                nullable: true,
                oldClrType: typeof(decimal),
                oldMaxLength: 128);

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultOtherCost",
                table: "Suppliers",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultFreightCost",
                table: "Suppliers",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "SubsidiaryAccountsReceivableDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "SubsidiaryAccountsReceivableDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "SubsidiaryAccountsPayableDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "SubsidiaryAccountsPayableDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Balance",
                table: "SubsidiaryAccountsPayableDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Saldo",
                table: "ReportIncomeStatements",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Saldo",
                table: "ReportBalanceSheets",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "ReportBalanceSheets",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "ReportBalanceSheets",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeValueDebet",
                table: "GeneralLedgerDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeValueCredit",
                table: "GeneralLedgerDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeRate",
                table: "GeneralLedgerDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "GeneralLedgerDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "GeneralLedgerDetails",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseValue",
                table: "Currencys",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseRate",
                table: "Currencys",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPayment",
                table: "CashReceipts",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPayment",
                table: "CashPayments",
                type: "decimal(24, 2)",
                nullable: false,
                oldClrType: typeof(decimal));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultVAT",
                table: "Suppliers",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultOtherTaxRemark",
                table: "Suppliers",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 128,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultOtherTax",
                table: "Suppliers",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultOtherCostRemark",
                table: "Suppliers",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 128,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultOtherCost",
                table: "Suppliers",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultFreightCost",
                table: "Suppliers",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "SubsidiaryAccountsReceivableDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "SubsidiaryAccountsReceivableDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "SubsidiaryAccountsPayableDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "SubsidiaryAccountsPayableDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Balance",
                table: "SubsidiaryAccountsPayableDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Saldo",
                table: "ReportIncomeStatements",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Saldo",
                table: "ReportBalanceSheets",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "ReportBalanceSheets",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "ReportBalanceSheets",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeValueDebet",
                table: "GeneralLedgerDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeValueCredit",
                table: "GeneralLedgerDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "ExchangeRate",
                table: "GeneralLedgerDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Debet",
                table: "GeneralLedgerDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Credit",
                table: "GeneralLedgerDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseValue",
                table: "Currencys",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "BaseRate",
                table: "Currencys",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPayment",
                table: "CashReceipts",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPayment",
                table: "CashPayments",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(24, 2)");
        }
    }
}
