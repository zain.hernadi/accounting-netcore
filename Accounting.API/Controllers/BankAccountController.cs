/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Saturday, April 20, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using System.Threading.Tasks;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Accounting.API.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class BankAccountController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        public BankAccountController(IMediator mediator, IConfiguration configuration)
        {
            _mediator = mediator;
            _configuration = configuration;
        }

        [Route("api/accounting/bankaccount/listpaging")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListPaging([FromQuery]Features.BankAccount.ListPaging.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/bankaccount/list")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> List([FromQuery]Features.BankAccount.List.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/bankaccount/get")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]Features.BankAccount.Get.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/bankaccount/submit")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Submit([FromBody]Features.BankAccount.Submit.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/bankaccount/delete")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Delete([FromBody]Features.BankAccount.Delete.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
    }
}
    
    
