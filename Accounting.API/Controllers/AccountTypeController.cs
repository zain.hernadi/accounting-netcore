/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using System.Threading.Tasks;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Accounting.API.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class AccountTypeController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        public AccountTypeController(IMediator mediator, IConfiguration configuration)
        {
            _mediator = mediator;
            _configuration = configuration;
        }

        [Route("api/accounting/accounttype/listpaging")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListPaging([FromQuery]Features.AccountType.ListPaging.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/accounttype/list")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> List([FromQuery]Features.AccountType.List.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/accounttype/get")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]Features.AccountType.Get.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/accounttype/submit")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Submit([FromBody]Features.AccountType.Submit.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/accounttype/delete")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Delete([FromBody]Features.AccountType.Delete.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
    }
}
    
    
