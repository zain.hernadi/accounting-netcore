﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Accounting.API.Common.Parameter
{
    public class Paging
    {
        public Paging()
        {
            Start = 0;
            Limit = 10;
            // HACK CONFIGURABLE
            Direction = DEFAULT_SORT_DIRECTION;
            SortColumn = DEFAULT_SORT_COLUMN;
        }

        public static Paging Instance()
        {
            return new Paging();
        }

        public const string DEFAULT_SORT_COLUMN = "ModifiedDate";
        public const string DEFAULT_SORT_DIRECTION = "DESC";
        public const int DEFAULT_PAGING = 20;
        public const int DEFAULT_PAGING_MAX = 1000000;
        public static Paging Instance(int start)
        {
            return new Paging
            {
                Start = start,
                Limit = 10,
                // HACK CONFIGURABLE
                Direction = DEFAULT_SORT_DIRECTION,
                SortColumn = DEFAULT_SORT_COLUMN
            };
        }

        public static Paging Instance(int start, int limit)
        {
            return new Paging
            {
                Start = start,
                Limit = limit,
                // HACK CONFIGURABLE
                Direction = DEFAULT_SORT_DIRECTION,
                SortColumn = DEFAULT_SORT_COLUMN
            };
        }

        public static Paging Instance(int start, int limit, string direction)
        {
            return new Paging
            {
                Start = start,
                Limit = limit,
                // HACK CONFIGURABLE
                Direction = GetValue(direction, DEFAULT_SORT_DIRECTION),
                SortColumn = DEFAULT_SORT_COLUMN
            };
        }

        public static Paging Instance(int start, int limit, string direction, string sortColumn)
        {
            return new Paging
            {
                Start = start,
                Limit = limit,
                // HACK CONFIGURABLE
                Direction = GetValue(direction, DEFAULT_SORT_DIRECTION),
                SortColumn = GetValue(sortColumn, DEFAULT_SORT_COLUMN)
            };
        }

        private static string GetValue(string value, string defaultvalue)
        {
            switch (value)
            {
                case "RecordStatus":
                    return "RowStatus";
                case "":
                    return defaultvalue;
                case null:
                    return defaultvalue;
                default:
                    return value;
            }
            //return string.IsNullOrEmpty(value) ? defaultvalue : value;
        }

        public static string GetExtJsSortColumn(string column)
        {
            if (column.Contains("property") && column.Contains("direction"))
            {
                var extJsSortings = JsonConvert.DeserializeObject<List<ExtJsSorting>>(column);
                if (extJsSortings != null && extJsSortings.Count > 0)
                {
                    return extJsSortings[0].property;
                }
            }
            return column;
        }

        public int Start { get; set; }
        public int Limit { get; set; }
        public string Direction { get; set; }
        public string SortColumn { get; set; }

        class ExtJsSorting
        {
            public string property { get; set; }
            public string direction { get; set; }
        }

        public static string GetExtJsSortColumn(string column, string direction)
        {
            if (column.Contains("property") && column.Contains("direction"))
            {
                var extJsSortings = JsonConvert.DeserializeObject<List<ExtJsSorting>>(column);
                if (extJsSortings != null && extJsSortings.Count > 0)
                {
                    return extJsSortings[0].direction;
                }
            }
            return direction;
        }
    }
}
