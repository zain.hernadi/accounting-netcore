/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.Manager;
using Accounting.API.Common.Parameter;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using KCore.Common.Response;

namespace Accounting.API.Features.CustomerShipment.List
{
    public class Handler : BaseDataManager, IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var javascriptPagings = JsonConvert.DeserializeObject<List<JavascriptPaging>>(request.Sorters ?? "");
            var whereTermList = ListParameter.GetListParameter(request.Params);
            var whereterm = GetQueryParameterLinqV02(whereTermList.ToArray());
            var pagingCollection = PagingCollection.GetPagingCollection(Paging.Instance(request.Start, request.Limit, Paging.DEFAULT_SORT_DIRECTION, request.Sort), javascriptPagings);

            var CustomerShipments = await _dbContext.CustomerShipments.Where(whereterm, ListValue.ToArray()).OrderBy(PagingCollection.LinqPaging(pagingCollection)).ToListAsync();
            
            return ApiResult<Response>.Ok(new Response
            {
                ListCustomerShipment = CustomerShipments,
                msg = "Success",
                success = true,
                result = true,
            });
        }
    }
}
