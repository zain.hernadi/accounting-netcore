/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using KCore.Common.Response;

namespace Accounting.API.Features.ChartOfAccount.Delete
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _dbContext.Database.BeginTransaction())
                {

                    var model = await _dbContext.ChartOfAccounts.FirstOrDefaultAsync(c => c.Id == new Guid(request.Id) && c.RowStatus == 0);
                    if (model == null)
                    {
                        return ApiResult<Response>.NotFound("Data Chart Of Account not found!");
                    }
                    model.RowStatus = 1;
                    model.ModifiedBy = request.User;
                    model.ModifiedDate = DateTime.Now;
                    _dbContext.ChartOfAccounts.Update(model);

                    await _dbContext.SaveChangesAsync();
                    scope.Commit();

                    return ApiResult<Response>.Ok(new Response
                    {
                        msg = "Delete Chart Of Account Success",
                        success = true,
                        result = true,
                    });
                }
            }
            catch (Exception ex)
            {
                return ApiResult<Response>.ErrorDuringProcessing(ex.Message);
            }
        }
    }
}
