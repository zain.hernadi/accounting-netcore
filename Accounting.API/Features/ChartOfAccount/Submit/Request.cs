/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using System;
using MediatR;
using Accounting.API.Common.API;
using KCore.Common.Response;

namespace Accounting.API.Features.ChartOfAccount.Submit
{
    public class Request : BaseSubmitRequest, IRequest<ApiResult<Response>>
    {
		public string SchemaCode { get; set; }
		public Guid? ParentId { get; set; }
		public bool IsAccountDetail { get; set; }
		public Guid AccountTypeId { get; set; }
		public string AccountCode { get; set; }
		public string AccountName { get; set; }
		public short AccountLevel { get; set; }
		public short? DefaultValue { get; set; }
		public string Notes { get; set; }
		public Guid CategoryAccountId { get; set; }
    }
}

