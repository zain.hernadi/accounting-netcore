﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class GeneralLedgerDetail : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "GeneralLedgerId")]
        public Guid GeneralLedgerId { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "AccountCode")]
        public string AccountCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "AccountName")]
        public string AccountName { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Debet")]
        public decimal Debet { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Credit")]
        public decimal Credit { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CurrencyCode")]
        public string CurrencyCode { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "ExchangeRate")]
        public decimal ExchangeRate { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "ExchangeValueDebet")]
        public decimal ExchangeValueDebet { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "ExchangeValueCredit")]
        public decimal ExchangeValueCredit { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }

        public virtual GeneralLedger GeneralLedger { get; set; }
    }
}
