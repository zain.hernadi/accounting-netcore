﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class CategoryCustomer : BaseContext
    {
        public CategoryCustomer()
        {
            Customers = new HashSet<Customer>();
        }

        public ICollection<Customer> Customers { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CategoryName")]
        public string CategoryName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "Priority")]
        public short Priority { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
