﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class AccountType : BaseContext
    {
        public AccountType()
        {
            ChartOfAccounts = new HashSet<ChartOfAccount>();
        }

        public ICollection<ChartOfAccount> ChartOfAccounts { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "AccountTypeCode")]
        public string AccountTypeCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "AccountTypeName")]
        public string AccountTypeName { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
