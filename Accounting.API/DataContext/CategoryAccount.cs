﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class CategoryAccount : BaseContext
    {
        public CategoryAccount()
        {
            ChartOfAccounts = new HashSet<ChartOfAccount>();
        }

        public ICollection<ChartOfAccount> ChartOfAccounts { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "AccountCategoryCode")]
        public string AccountCategoryCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "AccountCategoryName")]
        public string AccountCategoryName { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
