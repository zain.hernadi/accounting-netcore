﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class GeneralLedger : BaseApproveContext
    {
        public GeneralLedger()
        {
            GeneralLedgerDetails = new HashSet<GeneralLedgerDetail>();
        }

        public ICollection<GeneralLedgerDetail> GeneralLedgerDetails { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "FiscalYearCode")]
        public string FiscalYearCode { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "GeneralLedgerNo")]
        public string GeneralLedgerNo { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "GeneralLedgerName")]
        public string GeneralLedgerName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "GeneralLedgerDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime GeneralLedgerDate { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "GeneralLedgerType")]
        public string GeneralLedgerType { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ReffDocumentCode")]
        public string ReffDocumentCode { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ReffDocumentName")]
        public string ReffDocumentName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "TransactionDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime TransactionDate { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "TransactionType")]
        public string TransactionType { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CurrencyCode")]
        public string CurrencyCode { get; set; }

        [Required]
        [JsonProperty(PropertyName = "TotalDebet")]
        public decimal TotalDebet { get; set; }

        [Required]
        [JsonProperty(PropertyName = "TotalCredit")]
        public decimal TotalCredit { get; set; }

        [JsonProperty(PropertyName = "PostingDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime PostingDate { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsLastBalance")]
        public bool IsLastBalance { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
