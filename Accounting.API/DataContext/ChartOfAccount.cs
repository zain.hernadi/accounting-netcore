﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class ChartOfAccount : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "SchemaCode")]
        [StringLength(5)]
        public string SchemaCode { get; set; }

        [JsonProperty(PropertyName = "ParentId")]
        public Guid? ParentId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsAccountDetail")]
        public bool IsAccountDetail { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CategoryAccountId")]
        public Guid CategoryAccountId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountTypeId")]
        public Guid AccountTypeId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountCode")]
        [StringLength(24)]
        public string AccountCode { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountName")]
        [StringLength(128)]
        public string AccountName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountLevel")]
        public short AccountLevel { get; set; }

        [JsonProperty(PropertyName = "DefaultValue")]
        public short? DefaultValue { get; set; }

        [JsonProperty(PropertyName = "Notes")]
        [StringLength(512)]
        public string Notes { get; set; }


        public virtual CategoryAccount CategoryAccount { get; set; }
        public virtual AccountType AccountType { get; set; }
    }
}
